#!/bin/sh
# This is a comment

echo "Reiniciando app:"

HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

if [ "$1" = "dev" ];
then
    echo "Limpiando carpetas cache-logs"
    sudo rm -rf app/cache/*
    sudo rm -rf app/logs/*

    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:enmanuel:rwX app/cache app/logs
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:enmanuel:rwX app/cache app/logs
    php app/console cache:clear --env=$1

    echo "Assets y Assetics"
    php app/console assets:install web --env=$1 --symlink
    php app/console assetic:dump --env=$1
else

    echo "Limpiando carpetas cache-logs"
    rm -rf app/cache/*
    rm -rf app/logs/*

    setfacl -dR -m u:"$HTTPDUSER":rwX -m u:root:rwX app/cache app/logs
    setfacl -R -m u:"$HTTPDUSER":rwX -m u:root:rwX app/cache app/logs
    php app/console cache:clear --env=$1 --no-debug

    echo "Assets y Assetics"
    php app/console assets:install web --env=$1 --symlink
    php app/console assetic:dump --env=$1 --no-debug
fi

echo "Limpiando tabla de apuestas y reiniciando puntos, gemas, monedas"
mysql -h localhost -u$2 -p$3  $4 < dbscript.sql