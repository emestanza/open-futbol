function MainClass(e) {
    this.arrayBets = e;
    this.currentCoinsChecked = 0
}
MainClass.prototype = {constructor: function() {
        this.arrayBets = [];
        this.currentCoinsChecked = 0
    }, getBaseUrl: function() {
        try {
            var e = location.href;
            var t = e.indexOf("//");
            if (t < 0)
                t = 0;
            else
                t = t + 2;
            var n = e.indexOf("/", t);
            if (n < 0)
                n = e.length - t;
            var r = e.substring(t, n);
            return"http://" + r
        } catch (i) {
            return null
        }
    }, initSlider: function(e) {
        $(e).bxSlider({pager: false, minSlides: 1, maxSlides: 4, slideWidth: 200})
    }, initSlider2: function(e) {
        $(e).owlCarousel();
        var t = 0;
        var n = 0;
        $("img", "div.owl-item > div").each(function(e) {
            if ($(this).height() > t) {
                t = $(this).height()
            }
            if ($(this).width() > n) {
                n = $(this).width()
            }
        });
        $("div.owl-item > div").height(t).width(n)
    }, initSelect2: function(e, t) {
        $(e).select2({placeholder: t, allowClear: true, formatNoMatches: function() {
                return"No hay opciones"
            }})
    }, initpopOver: function(e, t, n) {
        $(e).popover({trigger: "hover", html: true, title: function() {
                return $(t).html()
            }, content: function() {
                return $(n).html()
            }, placement: "top"})
    }, initCountdown: function(e) {
        var t = $(e);
        t.each(function(e) {
            var t = $(this).data("matchdate");
            var n = $(this).data("matchid");
            $(this).countdown({date: t, render: function(e) {
                    if (e.days == 0 && e.hours == 0 && e.min == 0 && e.sec == 0) {
                        $(this.el).html("");
                        $(this.el).prev().html("CERRADAS LAS APUESTAS")
                    } else
                        $(this.el).html("<div>" + this.leadingZeros(e.days, 3) + " <span>días</span></div><div>" + this.leadingZeros(e.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(e.min, 2) + " <span>mins</span></div><div>" + this.leadingZeros(e.sec, 2) + " <span>segs</span></div>")
                }, onEnd: function() {
                    var e = $(this.el);
                    $.post(MainClass.prototype.getBaseUrl() + "/closematch", {matchId: n}).done(function(t) {
                        e.html("");
                        e.prev().html("CERRADAS LAS APUESTAS");
                        e.parent().addClass("close-bets");
                        e.parent().prev().find("input").hide();
                        e.parent().parent().parent().next().hide()
                    })
                }})
        })
    }, 
    initModal: function(e) {
        $(e).modal("hide")
    },
    initModalShow: function(e) {
        $(e).modal("show");
    }
    , initRestrictOnlyNumbers: function(e) {
        $(e).keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
                return false
        })
    }, initEvents: function(e, t) {
        var n = this;
        if ($.inArray("click", t) != -1) {
            $(document).on("click", e, function(t) {
                if (e == "#matches-tab") {
                    t.preventDefault();
                    $(".alert-warning").show()
                }
                if (e == ".bet-check") {
                    if ($(this).prop("checked")) {
                        var r = true;
                        var i = $(this).parent().parent().prev().find("input");
                        i.each(function() {
                            if ($.trim($(this).val()) == "") {
                                $(this).focus();
                                $(this).addClass("input-score-error");
                                r = false;
                                t.preventDefault()
                            } else {
                                $(this).removeClass("input-score-error")
                            }
                        });
                        if (r) {
                            var s = $("#user-current-coins").html();
                            s = s - $(this).data("coins");
                            if (s >= 0) {
                                $("#user-current-coins").html(s);
                                n.currentCoinsChecked = n.currentCoinsChecked + $(this).data("coins");
                                $("#confirmBetBtn").removeAttr("disabled");
                                $("#confirmBetBtn").addClass("animated tada");
                                var o = document.getElementById("audio");
                                o.play()
                            } else {
                                $("#moreCoinsModal").modal({backdrop: "static", keyboard: false});
                                return false
                            }
                        }
                    } else {
                        var s = $("#user-current-coins").html();
                        s = parseInt(s) + parseInt($(this).data("coins"));
                        $("#user-current-coins").html(s);
                        n.currentCoinsChecked = n.currentCoinsChecked - $(this).data("coins");
                        var u = true;
                        $(e).each(function() {
                            if ($(this).prop("checked")) {
                                u = false
                            }
                        });
                        if (u) {
                            $("#confirmBetBtn").attr("disabled", "disabled")
                            $("#confirmBetBtn").removeClass("animated tada");
                        }

                    }
                }
                if (e == ".more-match-btn") {
                    t.preventDefault();
                    $("#refresh-icon").hide();
                    $("#loading-icon").show();
                    var a = $(this).data("champid");
                    var f = $(".future_date").length;
                    $.get(MainClass.prototype.getBaseUrl() + "/getmorematches/" + a + "/" + f).done(function(e) {
                        $("#loading-icon").hide();
                        $("#refresh-icon").show();
                        $("#more-match-content").append(e);
                        MainClass.prototype.initCountdown(".future_date")
                    })
                }
                if (e == ".more-bets-btn") {
                    t.preventDefault();
                    $("#refresh-icon").hide();
                    $("#loading-icon").show();
                    var a = $(this).data("champid");
                    var l = $("#betTable");
                    var f = l.find("tbody tr").length;
                    $.get(MainClass.prototype.getBaseUrl() + "/getmorebets/" + a + "/" + f).done(function(e) {
                        $("#loading-icon").hide();
                        $("#refresh-icon").show();
                        l.find("tbody").append(e)
                    })
                }
                if (e == ".confirmRegModal") {
                    var c = $(this).data("champname");
                    var h = $(this).data("champdesc");
                    var a = $(this).data("champid");
                    var p = $(this).children(":first");
                    $("#champ-id").val(a);
                    $("#champ-name").html(c);
                    $("#champ-desc").html(h);
                    $("#champ-img").attr("src", p.attr("src"));
                    $("#champ-").attr("src", p.attr("src"));
                    $("#confirmRegModal").modal("show")
                }
                if (e == "#confirmBetBtn") {
                    var d = $("#totalReqCoins");
                    d.html(n.currentCoinsChecked);
                    var v = $("#totalAvCoins");
                    v.html($("#user-current-coins-hidden").val());
                    var m = $("#totalRemCoins");
                    m.html(parseInt(v.html()) - parseInt(d.html()));
                    $("#confirmBetModal").modal("show")
                }
                if (e == "#startBetBtn") {
                    $("#challengeZone").html("");
                    $("#nextQuestionBtn").hide();
                    $("#questionLoader").show();
                    $.get(MainClass.prototype.getBaseUrl() + "/challenges").done(function(e) {
                        if (e.indexOf("No hay preguntas") > -1) {
                            $("#nextQuestionBtn").hide()
                            $("#confirmAnswerBtn").hide();
                        } else{
                            $("#confirmAnswerBtn").show();
                            $("#nextQuestionBtn").show();
                        }
                            
                        $("#questionLoader").hide();
                        
                        if ($(e).find("#openAnswerTxt").length > 0)
                            $("#confirmAnswerBtn").removeClass("disabled");
                        
                        $("#challengeZone").append(e);
                    });
                    $("#challengeModal").modal({backdrop: "static", keyboard: false})
                }
                if (e == "#nextQuestionBtn") {

                    $("#confirmAnswerBtn").hide();
                    var g = $("#questionId").val();
                    $("#challengeContent").html("");
                    $("#questionLoader").show();
                    $.get(MainClass.prototype.getBaseUrl() + "/getnewquestion/" + g).done(function(e) {
                        var t = $(e).find("#endChallenge");
                        if (t.html() != undefined) {
                            $("#nextQuestionBtn").hide()
                        }
                        $("#questionLoader").hide();
                        $("#confirmAnswerBtn").addClass("disabled");
                        $("#confirmAnswerBtn").show();
                        $("#challengeContent").append(e)
                    })
                }
                if (e == "#newChallengeBtn") {
                    $("#startBetBtn").trigger("click");
                    $("#nextQuestionBtn").show()
                }
                if (e == "#alertNoCoins") {
                    $("#startBetBtn").trigger("click")
                }
                if (e == "#newCoinsBtn") {
                    $("#startBetBtn").trigger("click")
                }
                if (e == "#moreCoinsModalBtn") {
                    $("#startBetBtn").trigger("click")
                }
                if (e == "#finishChallengeBtn") {
                    $("#confirmBetBtn").trigger("click")
                }
                if (e == "#confirmAnswerBtn") {

                    t.preventDefault();
                    $(this).hide();
                    var answers = $(".answer-selected");
                    var ansIds = [];

                    answers.each(function() {
                        ansIds.push($(this).data("ansid"));
                    });

                    if (ansIds.length == 0){
                        var aux = $("#openAnswerTxt");
                        
                        if (aux.length > 0){
                            aux.attr("disabled", "disabled"); 
                            ansIds.push("open");
                            ansIds.push($("#questionId").val());
                            ansIds.push(aux.val());
                        }
                    }
                    
                   $.post(MainClass.prototype.getBaseUrl() + "/processanswer", {ansId: "" + ansIds.join("-")}).done(function(e) {

                        if (parseInt(e.coins) > 0) {

                            $(".answer-option").each(function() {
                                if ($(this).hasClass("active")) {
                                    $(this).removeClass("answer-selected");
                                    $(this).removeClass("active");
                                    $(this).addClass("list-group-item-success");
                                }
                                else
                                    $(this).addClass("disabled")
                            });

                            var t = $("#user-current-coins-hidden");
                            var n = parseInt(t.val()) + parseInt(e.coins);
                            $("#user-current-coins").html(parseInt($("#user-current-coins").html()) + e.coins);
                            t.val(n);
                            var r = document.getElementById("audio");
                            r.play();
                        }
                        else {

                            $(".answer-option").each(function() {
                                
                                if ($(this).hasClass("active")) {
                                    $(this).removeClass("answer-selected");
                                    $(this).removeClass("active");
                                    $(this).addClass("list-group-item-danger");
                                }
                                else
                                    $(this).addClass("disabled")
                            });

                        }

                    });

                }
                if (e == ".answer-option") {

                    t.preventDefault();

                    if (!$(this).hasClass("disabled") && !$(this).hasClass("list-group-item-success")) {
                        if (!$(this).hasClass("active")) {
                            $(this).addClass("answer-selected");
                            $(this).addClass("active");
                            $("#confirmAnswerBtn").removeClass("disabled");
                        }
                        else {
                            $(this).removeClass("answer-selected");
                            $(this).removeClass("active");

                            var answers = $(".answer-selected");
                            if (answers.length == 0)
                                $("#confirmAnswerBtn").addClass("disabled");

                        }
                    }
                }

                if (e == "a") {

                    var checks = $(".bet-check");
                    var betCounter = 0;
                    var itemsChecked = false;
                    $(checks).each(function() {
                        if ($(this).prop("checked")) {
                            itemsChecked = true;
                            betCounter++;
                        }
                    });

                    if (itemsChecked && $(this).attr("href") != "#") {
                        $("#keepBetCounter").html("" + betCounter);
                        $("#keepConfirmNoThanksBtn").attr("onclick", "window.location='" + $(this).attr("href") + "'");
                        $("#keepPageModal").modal({backdrop: "static", keyboard: false});
                        return false;
                    }

                }
            })
        }
        if ($.inArray("change", t) != -1) {
            $(e).change(function(t) {
                if (e == ".champList") {
                    window.location = $(".champList option:selected").val()
                }
                if (e == ".champNotRegList") {
                    var n = $(this).find(":selected");
                    var r = n.data("champname");
                    var i = n.data("champdesc");
                    var s = n.data("champid");
                    var o = n.data("champimg");
                    $("#champ-id").val(s);
                    $("#champ-name").html(r);
                    $("#champ-desc").html(i);
                    $("#champ-img").attr("src", o);
                    $("#confirmRegModal").modal("show")
                }
            })
        }
    }}