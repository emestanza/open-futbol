<?php

namespace Idigital\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class ChallengeController extends Controller
{

    /**
     * @Route("challenges")
     * @Template()
     */
    public function indexAction()
    {
        $userObj = $this->container->get('security.context')->getToken()->getUser();

        $answerPlayerRep = $this->getDoctrine()
                ->getRepository('BackendBundle:JugadorRespuesta');

        $lastAnswerObj = $answerPlayerRep->getLastAnswer($userObj->getId());

        $challengeRep = $this->getDoctrine()
                ->getRepository('BackendBundle:Reto');

        $challengeObj = null;
        $questionObj = null;

        // check if the current user has at least an answer in one challenge
        if ($lastAnswerObj != null) {

            //check for next question
            $lastQuestionObj = $lastAnswerObj->getRespuesta()->getPregunta();
            $questions = $lastQuestionObj->getReto()->getPreguntas();

            foreach ($questions as $key => $qObj) {

                if ($qObj->getId() == $lastQuestionObj->getId()) {
                    $questionObj = $questions->get($key + 1);
                    break;
                }
            }

            //if $questionObj is null it means when challenge has ended, and user has to
            // start a new challenge
            if ($questionObj == null) {

                //looking for new priority challenge
                $currentChallengeObj = $lastQuestionObj->getReto();
                $challengeObj = $challengeRep->getNewChallenge($currentChallengeObj, $userObj->getId());
                
                if ($challengeObj != null)
                    $questionObj = $challengeObj->getPreguntas()->get(0);

            }
            else $challengeObj = $questionObj->getReto();
            
        } else {
            //starting new challenge with 0 last answers
            $challengeObj = $challengeRep->getNewChallenge();
            $questionObj = $challengeObj->getPreguntas()->get(0);
        }

        return array(
            "challengeObj" => $challengeObj,
            "preguntaObj" => $questionObj
        );
    }

    /**
     * @Route("processanswer")
     * @Template()
     */
    public function processAnswerAction(Request $request)
    {

        $securityContext = $this->container->get('security.context');

        if ('POST' === $request->getMethod()) {

            header("Content-type: application/json");
            $answerRep = $this->getDoctrine()->getRepository('BackendBundle:RespuestaReto');
            $userObj = $this->container->get('security.context')->getToken()->getUser();
            $ansId = $request->request->get('ansId');
            $ansArr = explode("-", $ansId);
            $questionType = 0;
            $question = null;

            if (in_array("open", $ansArr)) {

                $questionType = 4;
                $questionRep = $this->getDoctrine()->getRepository('BackendBundle:PreguntaReto');
                $question = $questionRep->find($ansArr[1]);
            } else {

                $ansObj = $answerRep->find($ansArr[0]);
                $question = $ansObj->getPregunta();
                $questionType = $question->getTipo();
            }

            $isCorrect = false;
            $message = "";

            switch ($questionType) {
                case 1: // TIPO DE PREGUNTA SIMPLE
                    $isCorrect = $this->processSimpleQuestion($question, $ansArr);

                    if ($isCorrect)
                        $message = "RESPUESTA CORRECTA!";
                    else
                        $message = "RESPUESTA INCORRECTA!";

                    break;

                case 2: // TIPO DE PREGUNTA LIBRE
                    $isCorrect = true;
                    $message = "BUENA RESPUESTA!";

                    break;

                case 3: // TIPO DE PREGUNTA SELECCION MULTIPLE

                    $isCorrect = $this->processMultipleQuestion($question, $ansArr);

                    if ($isCorrect)
                        $message = "RESPUESTA CORRECTA!";
                    else
                        $message = "RESPUESTA INCORRECTA!";

                    break;
                case 4: // TIPO DE PREGUNTA ABIERTA
                    $isCorrect = true;
                    $this->processOpenQuestion($userObj, $ansArr);
                    $message = "BUENA RESPUESTA!";

                    break;
                case 5: // TIPO DE PREGUNTA VIDEO-TRIVIA
                    $isCorrect = $this->processVideoTriviaQuestion($question, $ansArr);

                    if ($isCorrect)
                        $message = "RESPUESTA CORRECTA!";
                    else
                        $message = "RESPUESTA INCORRECTA!";

                    break;
                    
                case 6: // TIPO DE PREGUNTA VIDEO-TRIVIA
                    $isCorrect = true;
                    $this->processOpenQuestion($userObj, $ansArr);
                    $message = "BUENA RESPUESTA!";
                    break;
            }

            $em = $this->getDoctrine()->getManager();

            if ($questionType != 4) {
                for ($i = 0; $i < sizeof($ansArr); $i++) {

                    $answerObj = $answerRep->find($ansArr[$i]);
                    $userAnsObj = new \Idigital\Bundle\BackendBundle\Entity\JugadorRespuesta();
                    $userAnsObj->setJugador($userObj);
                    $userAnsObj->setRespuesta($answerObj);
                    $em->persist($userAnsObj);
                    $em->flush();
                }
            }

            $this->sendJSONMessageAfterProcess($isCorrect, $message, $question, $userObj);
        } else
            die();
    }

    protected function processSimpleQuestion($question, $ansArr)
    {

        $questionAnsArr = $question->getRespuestas();
        $ansCorrectObj = null;

        foreach ($questionAnsArr as $ans) {
            if ($ans->getCorrecto()) {
                $ansCorrectObj = $ans;
                break;
            }
        }

        if (sizeof($ansArr) == 1 && $ansCorrectObj->getId() == intval($ansArr[0]))
            return true;
        else
            return false;
    }

    protected function processMultipleQuestion($question, $ansArr)
    {
        
        $questionAnsArr = $question->getRespuestas();
        $numAnswers = sizeof($ansArr);
        $numCorrect = 0;
        foreach ($questionAnsArr as $ans) {
            
            if ($ans->getCorrecto()) {
                
                $numCorrect++;
                
                if (in_array($ans->getId(), $ansArr)){
                    $key = array_search($ans->getId(), $ansArr);
                    unset($ansArr[$key]);
                }
                
            }
        }

        if (empty($ansArr) && $numCorrect == $numAnswers)
            return true;
        else
            return false;
    }

    protected function sendJSONMessageAfterProcess($isCorrect, $message, $question, $userObj)
    {
        $coins = 0;

        if ($isCorrect) {
            $coins = $question->getMonedasOtorgadas();
            $userObj->setMonedas($userObj->getMonedas() + $coins);
            $em = $this->getDoctrine()->getManager();
            $em->persist($userObj);
            $em->flush();
        }

        $arrayResult = array("coins" => $coins, "message" => $message);
        echo json_encode($arrayResult);
        exit();
    }

    /**
     * @Route("getnewquestion/{lastQuestionId}")
     * @Template()
     */
    public function getNewQuestionAction($lastQuestionId)
    {
        $questionRep = $this->getDoctrine()
                ->getRepository('BackendBundle:PreguntaReto');

        $questionObj = $questionRep->find($lastQuestionId);
        $questions = $questionObj->getReto()->getPreguntas();
        $aux = null;

        foreach ($questions as $key => $qObj) {

            if ($qObj->getId() == $questionObj->getId()) {
                $aux = $questions->get($key + 1);
                break;
            }
        }

        return array(
            "preguntaObj" => $aux
        );
    }

    protected function processOpenQuestion($userObj, $ansArr)
    {
        $em = $this->getDoctrine()->getManager();
        $questionRep = $this->getDoctrine()->getRepository('BackendBundle:PreguntaReto');
        $questionObj = $questionRep->find($ansArr[1]);
        
        $answerChallengeObj = new \Idigital\Bundle\BackendBundle\Entity\RespuestaReto();
        $answerChallengeObj->setCorrecto(true);
        $answerChallengeObj->setList(false);
        $answerChallengeObj->setPregunta($questionObj);
        $answerChallengeObj->setRespuesta("abierta");
        
        $em->persist($answerChallengeObj);
        $em->flush();
        
        $openAnwerObj = new \Idigital\Bundle\BackendBundle\Entity\JugadorRespuesta();
        $openAnwerObj->setJugador($userObj);
        $openAnwerObj->setRespuesta($answerChallengeObj);
        $openAnwerObj->setRespuestaAbierta($ansArr[2]);

        $em->persist($openAnwerObj);
        $em->flush();
    }

    
    protected function processVideoTriviaQuestion($question, $ansArr)
    {

        $questionAnsArr = $question->getRespuestas();
        $ansCorrectObj = null;

        foreach ($questionAnsArr as $ans) {
            if ($ans->getCorrecto()) {
                $ansCorrectObj = $ans;
                break;
            }
        }

        if (sizeof($ansArr) == 1 && $ansCorrectObj->getId() == intval($ansArr[0]))
            return true;
        else
            return false;
    }
}
