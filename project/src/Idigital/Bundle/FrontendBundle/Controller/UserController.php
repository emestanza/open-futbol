<?php

namespace Idigital\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UserController extends Controller
{

    /**
     * @Route("progress")
     * @Template()
     */
    public function progressAction()
    {

        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

            $userRep = $this->getDoctrine()
                    ->getRepository('ApplicationSonataUserBundle:User');

            $userObj = $this->container->get('security.context')->getToken()->getUser();

            if (!$userObj) {
                throw $this->createNotFoundException(
                        'Jugador no encontrado '
                );
            }

            $fbookFriendsIdArr = array("0");
            $rankingFbook = array();
            $userFbookRanking = array();

            if ($userObj->getFacebookData() != "") {
                $facebook = $this->get('facebook');
                $facebook->setAccessToken($userObj->getFacebookData());

                $friends = $facebook->api('/me/friends');

                foreach ($friends["data"] as $friendObj) {

                    $fbookFriendsIdArr[] = $friendObj["id"];
                }

                $rankingFbook = $userRep->findTopTenFbook($userObj->getId(), $fbookFriendsIdArr);
                $userFbookRanking = $userRep->getPlayerFbookRaking($userObj->getId(), $fbookFriendsIdArr);
            }

            $totalPlayers = $userRep->countPlayers();

            $ranking = $userRep->findTopTen();

            $userRanking = $userRep->getPlayerRaking($userObj->getId());


            return array("userObj" => $userObj,
                "ranking" => $ranking,
                "rankingFbook" => $rankingFbook,
                "userRanking" => $userRanking,
                "userFbookRanking" => $userFbookRanking,
                "totalPlayers" => $totalPlayers,
                "fbookRankingColors" => $this->getFbookRankingColors(),
                "countryArr" => \Idigital\Bundle\BackendBundle\Admin\Utils::$countries
            );
        } else {
            throw $this->createNotFoundException(
                    'Pagina no Disponible'
            );
        }
    }

    /**
     * @Route("bets/{champId}")
     * @Template()
     */
    public function betsAction($champId = 0)
    {

        $userRep = $this->getDoctrine()
                ->getRepository('ApplicationSonataUserBundle:User');

        $userObj = $this->container->get('security.context')->getToken()->getUser();

        $champsRegistered = $userObj->getCampeonatos();

        $ligaRep = $this->getDoctrine()
                ->getRepository('BackendBundle:LigaCampeonato');

        $betRep = $this->getDoctrine()
                ->getRepository('BackendBundle:Apuesta');

        if ($champId == 0) {
            $champObj = null;
            $allBets = $betRep->getAllBetsByUser($userObj->getId(), 0, false);
            $userBets = $betRep->getAllBetsByUser($userObj->getId());
        } else {
            $champObj = $ligaRep->find($champId);

            if ($champObj != null) {
                $allBets = $betRep->getBetsByChamp($userObj->getId(), $champObj->getId(), 0, false);
                $userBets = $betRep->getBetsByChamp($userObj->getId(), $champObj->getId());
            } else {
                throw $this->createNotFoundException(
                        'Campeonato no existe'
                );
            }
        }

        $totalPoints = 0;
        $currentPoints = $userRep->getCurrentPoints($userObj);

        foreach ($userBets as $betObj) {

            $partidoObj = $betObj->getPartido();

            if ($partidoObj->getCerrado()) {

                //general
                if ($betObj->getApuestaScore1() == $partidoObj->getScoreUno() && $betObj->getApuestaScore2() == $partidoObj->getScoreDos())
                    $totalPoints = $totalPoints + $this->container->getParameter("bet_general_points");

                //ganador
                if ($betObj->getApuestaScore1() == $partidoObj->getScoreUno() || $betObj->getApuestaScore2() == $partidoObj->getScoreDos())
                    $totalPoints = $totalPoints + $this->container->getParameter("bet_winner_points");

                //parcial
                if ($betObj->getApuestaScore1() < $partidoObj->getScoreUno() || $betObj->getApuestaScore2() < $partidoObj->getScoreDos())
                    $totalPoints = $totalPoints + $this->container->getParameter("bet_partial_points");
            }
        }

        return array("userObj" => $userObj,
            "champsReg" => $champsRegistered,
            "champObj" => $champObj,
            "userBets" => $userBets,
            "allBets" => $allBets,
            "totalPoints" => $totalPoints,
            "coinsGeneral" => $this->container->getParameter("bet_general_coins"),
            "coinsWinner" => $this->container->getParameter("bet_winner_coins"),
            "coinsPartial" => $this->container->getParameter("bet_partial_coins"),
            "pointsGeneral" => $this->container->getParameter("bet_general_points"),
            "pointsWinner" => $this->container->getParameter("bet_winner_points"),
            "pointsPartial" => $this->container->getParameter("bet_partial_points"),
        );
    }

    /**
     * @Route("getmorebets/{champId}/{offset}")
     * @Template()
     */
    public function getMoreBetsAction($champId, $offset)
    {

        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {

            $userObj = $this->container->get('security.context')->getToken()->getUser();

            $betRep = $this->getDoctrine()
                    ->getRepository('BackendBundle:Apuesta');

            if ($champId == 0)
                $userBets = $betRep->getAllBetsByUser($userObj->getId(), $offset, true);
            else
                $userBets = $betRep->getBetsByChamp($userObj->getId(), $champId, $offset);

            return array("bets" => $userBets,
                "champId" => $champId,
                "coinsGeneral" => $this->container->getParameter("bet_general_coins"),
                "coinsWinner" => $this->container->getParameter("bet_winner_coins"),
                "coinsPartial" => $this->container->getParameter("bet_partial_coins"),
                "pointsGeneral" => $this->container->getParameter("bet_general_points"),
                "pointsWinner" => $this->container->getParameter("bet_winner_points"),
                "pointsPartial" => $this->container->getParameter("bet_partial_points"),
                "offset" => $offset,);
        } else {
            throw $this->createNotFoundException(
                    'Pagina no Disponible'
            );
        }
    }

    protected function getFbookRankingColors()
    {

        return array("#398FB3",
            "#7CD9FF",
            "#B3B3B3",
            "#BCBD3D",
            "#B02A27",
            "#0F4E7D",
            "#2BA4FC",
            "#F9EFEB",
            "#148C79",
            "#053E33",
        );
    }

}
