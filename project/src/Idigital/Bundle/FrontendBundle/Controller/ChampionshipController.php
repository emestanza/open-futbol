<?php

namespace Idigital\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Idigital\Bundle\BackendBundle\Entity\Apuesta;
use Symfony\Component\HttpFoundation\Session\Session;

class ChampionshipController extends Controller
{

    /**
     * @Route("championships")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $userObj = $this->container->get('security.context')->getToken()->getUser();

        $userRep = $this->getDoctrine()
                ->getRepository('ApplicationSonataUserBundle:User');

        if (!$userObj) {
            throw $this->createNotFoundException(
                    'Jugador no encontrado '
            );
        }

        $champsRegistered = $userObj->getCampeonatos();
        $champsNotRegistered = $userRep->findChampsNotRegistered($champsRegistered);

        $prizesRep = $this->getDoctrine()
                ->getRepository('BackendBundle:Marca');

        $welcome = false;
        if ($request->query->get('welcome') != null) {
            $welcome = true;
        }

        return array(
            "champsReg" => $champsRegistered,
            "champsNotReg" => $champsNotRegistered,
            "marcas" => $prizesRep->findBy(array("deshabilitado" => 0)),
            "userObj" => $userObj,
            "welcome" => $welcome,
        );
    }

    /**
     * @Route("registerchamp")
     * @Template()
     */
    public function registerChampAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userRep = $this->getDoctrine()
                ->getRepository('ApplicationSonataUserBundle:User');

        $userObj = $this->container->get('security.context')->getToken()->getUser();

        if (!$userObj) {
            throw $this->createNotFoundException(
                    'Jugador no encontrado '
            );
        }

        $ligaRep = $this->getDoctrine()
                ->getRepository('BackendBundle:LigaCampeonato');

        $champId = $this->get('request')->request->get('champId');
        $champObj = $ligaRep->find($champId);
        $userObj->addCampeonato($champObj);
        $em->persist($userObj);
        $em->flush();
        return $this->redirect($this->generateUrl('idigital_frontend_championship_index'));
    }

    /**
     * @Route("matches/{champId}")
     * @Template()
     */
    public function matchesAction(Request $request, $champId)
    {
        if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))
            return $this->redirect($this->generateUrl('sonata_user_security_login'));
        else {

            $session = $request->getSession();
            $session->set('champ_match_id', $champId);

            $userObj = $this->container->get('security.context')->getToken()->getUser();

            $matchesUserArr = $userObj->getApuestas();
            $matchesUserArr = $matchesUserArr->map(function($entity) {
                        return $entity->getPartido()->getId();
                    })->toArray();

            $matchesUserArr[] = 0;

            $ligaRep = $this->getDoctrine()
                    ->getRepository('BackendBundle:LigaCampeonato');

            $matchRep = $this->getDoctrine()
                    ->getRepository('BackendBundle:Partido');

            $partidos = $matchRep->getPartidosBatchSix($champId, 0, $matchesUserArr, true);
            $partidosAll = $matchRep->getPartidosBatchSix($champId, 0, $matchesUserArr, false);

            $champObj = $ligaRep->find($champId);
            $moreThanSix = sizeof($partidosAll) > 6;

            return array(
                "champObj" => $champObj,
                "partidos" => $partidos,
                "moreThanSix" => $moreThanSix,
                "userObj" => $userObj,
                "coinsGeneral" => $this->container->getParameter("bet_general_coins"),
                "coinsWinner" => $this->container->getParameter("bet_winner_coins"),
                "coinsPartial" => $this->container->getParameter("bet_partial_coins"),
                "pointsGeneral" => $this->container->getParameter("bet_general_points"),
                "pointsWinner" => $this->container->getParameter("bet_winner_points"),
                "pointsPartial" => $this->container->getParameter("bet_partial_points"),
            );
        }
    }

    /**
     * @Route("getmorematches/{champId}/{offset}")
     * @Template()
     */
    public function getMoreMatchesAction($champId, $offset)
    {

        $userObj = $this->container->get('security.context')->getToken()->getUser();

        $matchesUserArr = $userObj->getApuestas();
        $matchesUserArr = $matchesUserArr->map(function($entity) {
                    return $entity->getPartido()->getId();
                })->toArray();

        $matchesUserArr[] = 0;

        $matchRep = $this->getDoctrine()
                ->getRepository('BackendBundle:Partido');

        $partidos = $matchRep->getPartidosBatchSix($champId, $offset, $matchesUserArr, true);

        return array(
            "partidos" => $partidos,
            "coinsGeneral" => $this->container->getParameter("bet_general_coins"),
            "coinsWinner" => $this->container->getParameter("bet_winner_coins"),
            "coinsPartial" => $this->container->getParameter("bet_partial_coins"),
            "pointsGeneral" => $this->container->getParameter("bet_general_points"),
            "pointsWinner" => $this->container->getParameter("bet_winner_points"),
            "pointsPartial" => $this->container->getParameter("bet_partial_points"),
        );
    }

    /**
     * @Route("closematch")
     * @Template()
     */
    public function closeMatchAction(Request $request)
    {
        $matchId = $request->request->get('matchId');

        $em = $this->getDoctrine()->getManager();
        $matchRep = $this->getDoctrine()
                ->getRepository('BackendBundle:Partido');

        $matchObj = $matchRep->find($matchId);
        $matchObj->setCerrado(true);
        $em->persist($matchObj);
        $em->flush();
        exit();
    }

    /**
     * @Route("confirmbets")
     * @Template()
     */
    public function confirmBetsAction(Request $request)
    {

        if ('POST' === $request->getMethod()) {

            $userObj = $this->container->get('security.context')->getToken()->getUser();

            $matchRep = $this->getDoctrine()
                    ->getRepository('BackendBundle:Partido');

            $em = $this->getDoctrine()->getManager();

            $bets = $request->request->get('bets');

            $numBetsDone = 0;
            $matchResume = "";

            foreach ($bets as $key => $betObj) {

                if (isset($betObj["'isChecked'"])) {

                    $newBetObj = new Apuesta();
                    $newBetObj->setApuestaScore1($betObj["'team1-score'"]);
                    $newBetObj->setApuestaScore2($betObj["'team2-score'"]);
                    $newBetObj->setJugador($userObj);
                    $newBetObj->setGano(false);

                    $key = str_replace("'", "", $key);
                    $matchObj = $matchRep->find($key);
                    $newBetObj->setPartido($matchObj);

                    $em->persist($newBetObj);
                    $em->flush();
                    $userObj->setMonedas($userObj->getMonedas() - $betObj["'isChecked'"]);

                    $em->persist($userObj);
                    $em->flush();

                    $numBetsDone++;
                    $aux1 = $betObj["'team1-score'"];
                    $aux2 = $betObj["'team2-score'"];
                    $matchResume = $matchResume . "<h4 class='text-uppercase'>" . $matchObj->getEquipoUno()->getNombre() . "  " . $aux1 . " - " . $aux2 . "  " . $matchObj->getEquipoDos()->getNombre() . "</h4>";
                }
            }

            if ($numBetsDone > 1)
                $numBetsDone = "Sus " . $numBetsDone . " apuestas han sido confirmadas!.";
            else
                $numBetsDone = "Su apuesta ha sido confirmada!.";

            $session = $request->getSession();
            $session->getFlashBag()->add('numBetsDone', $numBetsDone);
            $session->getFlashBag()->add('betResume', $matchResume);
            $response = $this->redirect('matches/' . $session->get('champ_match_id'));

            return $response;
        }
        else {
            throw $this->createNotFoundException(
                    'Página no Disponible'
            );
        }
    }

}
