<?php

namespace Idigital\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirect($this->generateUrl('idigital_frontend_championship_index'));
        } else {

            $betRep = $this->getDoctrine()->getRepository('BackendBundle:Apuesta');
            
            $betArr = $betRep->findAll();
            
            $betSplit = str_split(strval(sizeof($betArr)));
            
            if (sizeof($betSplit) < 5){
                for ($i = 0; $i <= (5 - sizeof($betSplit)); $i++){
                    array_unshift($betSplit, "0");
                }
            }
            
            $contentRep = $this->getDoctrine()->getRepository('BackendBundle:IndexContent');
            $content = $contentRep->find(1);
            $form = $this->container->get('sonata.user.registration.form');

            return array(
                'betNumberArr' => $betSplit,
                'form' => $form->createView(),
                'content' => $content,
            );
        }
    }

    /**
     * @Route("/terms")
     * @Template()
     */
    public function termsAction()
    {
        return array(
        );
    }

    /**
     * @Route("/howto")
     * @Template()
     */
    public function howtoAction()
    {
        return array(
        );
    }

    /**
     * @Route("/prizes")
     * @Template()
     */
    public function prizesAction()
    {

        $prizesRep = $this->getDoctrine()
                ->getRepository('BackendBundle:MarcaPremio');

        $prizesArr = $prizesRep->findFirstFive();

        return array(
            "prizesArr" => $prizesArr
        );
    }

}
