<?php

namespace Idigital\Bundle\BackendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\Validation;
use Application\Sonata\UserBundle\Entity\User;

class DefaultControllerTest extends WebTestCase
{

    private $validator;

    protected function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()
                ->enableAnnotationMapping()
                ->getValidator();
    }

    public function testIndex()
    {
//        $client = static::createClient();
//
//        $crawler = $client->request('GET', '/hello/Fabien');
//
//        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
        $this->assertTrue(true);
    }

    //put your code here
    public function testUserData()
    {
        $userObj = new User();
        $userObj->setFirstname("Enmanuel");
        $userObj->setLastname("Mestanza");
        $userObj->setUsername("emestanza");
        //$userObj->setEmail("emestanza");
        //$userObj->setPassword("12345");

        $listaErrores = $this->validator->validate($userObj);
        $this->assertEquals(0, $listaErrores->count(), 'erroreeeeeeeees: ' . $listaErrores);
    }

    /** @test */
    public function testGoIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en portada');

        $homeLink = $crawler->filter('html:contains("Home")')->count();
        $howToLink = $crawler->filter('html:contains("Cómo Jugar")')->count();
        $prizesLink = $crawler->filter('html:contains("Premios")')->count();
        $loginLink = $crawler->filter('html:contains("Login")')->count();
        //
        $this->assertEquals(1, $homeLink, "Existe home link");
        $this->assertEquals(1, $prizesLink, "Existe premios link");
        $this->assertEquals(1, $howToLink, "Existe como jugar link");
        $this->assertEquals(1, $loginLink, "Existe login link");

        $howtoLinkObj = $crawler->selectLink('Cómo Jugar');
        $linkObj = $howtoLinkObj->link();
        $client->click($linkObj);
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en como jugar');

        $client->back();

        $howtoLinkObj = $crawler->selectLink('Premios');
        $linkObj = $howtoLinkObj->link();
        $client->click($linkObj);
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en premios');

        $client->back();

        $howtoLinkObj = $crawler->selectLink('Login');
        $linkObj = $howtoLinkObj->link();
        $client->click($linkObj);
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en login');

        $client->back();
    }

    /** @test */
    public function testRegularRegistration()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        //$crawler = $client->followRedirect();

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Status 200 en login');

        for ($i = 0; $i < 10; $i++) {
            $usuario = array(
                'sonata_user_registration_form[firstname]' => 'Anónimo',
                'sonata_user_registration_form[lastname]' => 'Apellido1 Apellido2',
                'sonata_user_registration_form[email]' => 'anonimo' . uniqid() . '@email.com',
                'sonata_user_registration_form[plainPassword][first]' => 'strtok',
                'sonata_user_registration_form[plainPassword][second]' => 'strtok',
            );

            $formulario = $crawler->selectButton('Registrarme')->form($usuario);
            $client->submit($formulario);

            $client = static::createClient();
            $crawler = $client->request('GET', '/');
        }
    }

    public function generaUsuarios()
    {

        for ($i = 0; $i < 10; $i++) {
            $usersArr[] = array(
                'sonata_user_registration_form[firstname]' => 'Anónimo',
                'sonata_user_registration_form[lastname]' => 'Apellido1 Apellido2',
                'sonata_user_registration_form[email]' => 'anonimo' . $i . '@email.com',
                'sonata_user_registration_form[plainPassword][first]' => 'strtok',
                'sonata_user_registration_form[plainPassword][second]' => 'strtok',
            );
        }

        return array(
            array(
                $usersArr
            )
        );
    }

}
