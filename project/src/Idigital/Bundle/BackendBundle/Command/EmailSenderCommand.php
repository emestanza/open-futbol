<?php

namespace Idigital\Bundle\BackendBundle\Command;

/**
 * Description of EmailSenderCommand
 *
 * @author enmanuel
 */
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailSenderCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('ofutbol:emailsender')
                ->setDescription('Command description')
                ->addArgument('my_argument', InputArgument::OPTIONAL, 'Argument description');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $userRep = $this->getContainer()->get('doctrine')->getRepository('ApplicationSonataUserBundle:User');
        $champRep = $this->getContainer()->get('doctrine')->getRepository('BackendBundle:Partido');
        $notifRep = $this->getContainer()->get('doctrine')->getRepository('BackendBundle:EmailNotification');
        $notifArr = $notifRep->findAll();
        $currentDate = new \DateTime();
        
        foreach ($notifArr as $notifObj) {

            //getting all the bets according the date obtained by backend
            $day = $currentDate->format("N");
            $hour = $currentDate->format("H");
            $minute = $currentDate->format("i");

            if ($day == strval($notifObj->getDay()) && $hour == $notifObj->getHour() && $minute == $notifObj->getMinute()) {

                $lastDay = new \DateTime('last day of this month');
                $interval = date_diff($currentDate, $lastDay);

                $allMatches = $champRep->getAllNextMatchesByCurrentDate($currentDate->format('Y-m-d H:i:s'), $lastDay->format('Y-m-d H:i:s'));

                $allTotalCoins = sizeof($allMatches) * 60;
                $allMatchesArr[] = 0;

                foreach ($allMatches as $value) {
                    $allMatchesArr[] = $value["id"];
                }

                $allUsers = $userRep->getPlayers();

                foreach ($allUsers as $userObj) {
                    $needMoreCoins = false;
                    $betsUserArr = $userObj->getApuestas();
                    $betsUserArr = $betsUserArr->map(function($entity) {
                                return $entity->getPartido()->getId();
                            })->toArray();

                    $betsUserArr[] = 0;
                    $nextMatches = $champRep->getNextMatchesFilteredByUser($allMatchesArr, $betsUserArr);

                    if ($userObj->getPuntos() < $allTotalCoins)
                        $needMoreCoins = true;

                    $baseUrl = "http://openfutbol.local.com";

                    if ($this->getContainer()->get('kernel')->getEnvironment() == "test")
                        $baseUrl = "http://pruebas.openfutbol.net";

                    if ($this->getContainer()->get('kernel')->getEnvironment() == "prod")
                        $baseUrl = "http://openfutbol.net";

                    $imgUrl = \Swift_Image::fromPath($baseUrl . '/bundles/frontend/images/email_logo.png');

                    $message = \Swift_Message::newInstance()
                            ->setSubject('Open Futbol - Newsletter')
                            ->setFrom('admin@openfutbol.com', "Staff Open Futbol")
                            ->setSender('admin@openfutbol.com', "Staff Open Futbol")
                            ->setTo($userObj->getEmail());

                    $auxImg = $message->embed($imgUrl);

                    $message->setBody(
                            $this->getContainer()->get('templating')->render(
                                    'BackendBundle:EmailTemplate:emailNotification.html.twig', array("userObj" => $userObj,
                                "needMoreCoins" => $needMoreCoins,
                                "nextMatches" => $nextMatches,
                                "allTotalCoins" => $allTotalCoins,
                                "logoImg" => $auxImg,
                                "interval" => $interval->format("%d"),
                                "baseUrl" => $baseUrl)
                            ), 'text/html');

                    $this->getContainer()->get('mailer')->send($message);
                }
            }
        }
    }

    //put your code here
}
