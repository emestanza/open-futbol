<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PreguntaReto
 *
 * @ORM\Table(name="pregunta_reto")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\PreguntaRetoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PreguntaReto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="text")
     */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="orden_prioridad", type="smallint")
     */
    private $ordenPrioridad;

    /**
     * @var string
     *
     * @ORM\Column(name="monedas_otorgadas", type="smallint")
     */
    private $monedasOtorgadas;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="smallint")
     */
    private $tipo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="limite_caracteres_resp", type="smallint", nullable = true)
     */
    private $limiteCaracteresRespuesta;

    /**
     * @var string
     *
     * @ORM\Column(name="iframe_video", type="text", nullable=true)
     */
    private $iframeVideo;
    
    /**
     * @ORM\ManyToOne(targetEntity="Reto", inversedBy="preguntas")
     * @ORM\JoinColumn(name="reto_id", referencedColumnName="id", nullable=false)
     * */
    private $reto;

    /**
     * @ORM\OneToMany(targetEntity="RespuestaReto", mappedBy="pregunta")
     */
    private $respuestas;

    public function __construct()
    {
        $this->respuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var boolean
     *
     * @ORM\Column(name="deshabilitado", type="boolean", options={"default" = 0})
     */
    private $deshabilitado = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     * @return PreguntaReto
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return PreguntaReto
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return PreguntaReto
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set reto
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Reto $reto
     * @return PreguntaReto
     */
    public function setReto(\Idigital\Bundle\BackendBundle\Entity\Reto $reto = null)
    {
        $this->reto = $reto;

        return $this;
    }

    /**
     * Get reto
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Reto 
     */
    public function getReto()
    {
        return $this->reto;
    }

    /**
     * Add respuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuestas
     * @return PreguntaReto
     */
    public function addRespuesta(\Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuestas)
    {
        $this->respuestas[] = $respuestas;

        return $this;
    }

    /**
     * Remove respuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuestas
     */
    public function removeRespuesta(\Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuestas)
    {
        $this->respuestas->removeElement($respuestas);
    }

    /**
     * Get respuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRespuestas()
    {
        return $this->respuestas;
    }

    public function __toString()
    {
        return $this->getPregunta();
    }

    /**
     * Set ordenPrioridad
     *
     * @param integer $ordenPrioridad
     * @return PreguntaReto
     */
    public function setOrdenPrioridad($ordenPrioridad)
    {
        $this->ordenPrioridad = $ordenPrioridad;

        return $this;
    }

    /**
     * Get ordenPrioridad
     *
     * @return integer 
     */
    public function getOrdenPrioridad()
    {
        return $this->ordenPrioridad;
    }

    /**
     * Set monedasOtorgadas
     *
     * @param integer $monedasOtorgadas
     * @return PreguntaReto
     */
    public function setMonedasOtorgadas($monedasOtorgadas)
    {
        $this->monedasOtorgadas = $monedasOtorgadas;

        return $this;
    }

    /**
     * Get monedasOtorgadas
     *
     * @return integer 
     */
    public function getMonedasOtorgadas()
    {
        return $this->monedasOtorgadas;
    }

    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return PreguntaReto
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return PreguntaReto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    /**
     * Set limiteCaracteresRespuesta
     *
     * @param integer $limiteCaracteresRespuesta
     * @return PreguntaReto
     */
    public function setLimiteCaracteresRespuesta($limiteCaracteresRespuesta)
    {
        $this->limiteCaracteresRespuesta = $limiteCaracteresRespuesta;

        return $this;
    }

    /**
     * Get limiteCaracteresRespuesta
     *
     * @return integer 
     */
    public function getLimiteCaracteresRespuesta()
    {
        return $this->limiteCaracteresRespuesta;
    }

    /**
     * Set iframeVideo
     *
     * @param string $iframeVideo
     * @return PreguntaReto
     */
    public function setIframeVideo($iframeVideo)
    {
        $this->iframeVideo = $iframeVideo;

        return $this;
    }

    /**
     * Get iframeVideo
     *
     * @return string 
     */
    public function getIframeVideo()
    {
        return $this->iframeVideo;
    }
}
