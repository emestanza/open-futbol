<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailTemplate
 *
 * @ORM\Table(name="email_template")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\EmailTemplateRepository")
 */
class EmailTemplate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="template_name", type="string", length=100)
     */
    private $templateName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email_content", type="text")
     */
    private $emailContent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailContent
     *
     * @param string $emailContent
     * @return EmailTemplate
     */
    public function setEmailContent($emailContent)
    {
        $this->emailContent = $emailContent;

        return $this;
    }

    /**
     * Get emailContent
     *
     * @return string 
     */
    public function getEmailContent()
    {
        return $this->emailContent;
    }


    /**
     * Set templateName
     *
     * @param string $templateName
     * @return EmailTemplate
     */
    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Get templateName
     *
     * @return string 
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }
}
