<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * MarcaPremio
 *
 * @ORM\Table(name="marca_premio")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\MarcaPremioRepository")
 * @ORM\HasLifecycleCallbacks
 */
class MarcaPremio extends BaseModel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;

    /**
     * object to be used for file uploading
     * @var type UpdateFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_premio", type="string", length=100, nullable = true)
     */
    public $fileName;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User", mappedBy="premios")
     * */
    private $ganadoresPremios;

    /**
     * @ORM\ManyToOne(targetEntity="Marca", inversedBy="premios")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id", nullable = false)
     * */
    private $marca;

    public function __construct()
    {
        $this->ganadoresPremios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return MarcaPremio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return MarcaPremio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return MarcaPremio
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return MarcaPremio
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Add ganadoresPremios
     *
     * @param \Application\Sonata\UserBundle\Entity\User $ganadoresPremios
     * @return MarcaPremio
     */
    public function addGanadoresPremio(\Application\Sonata\UserBundle\Entity\User $ganadoresPremios)
    {
        $this->ganadoresPremios[] = $ganadoresPremios;

        return $this;
    }

    /**
     * Remove ganadoresPremios
     *
     * @param \Application\Sonata\UserBundle\Entity\User $ganadoresPremios
     */
    public function removeGanadoresPremio(\Application\Sonata\UserBundle\Entity\User $ganadoresPremios)
    {
        $this->ganadoresPremios->removeElement($ganadoresPremios);
    }

    /**
     * Get ganadoresPremios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGanadoresPremios()
    {
        return $this->ganadoresPremios;
    }

    /**
     * Set marca
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Marca $marca
     * @return MarcaPremio
     */
    public function setMarca(\Idigital\Bundle\BackendBundle\Entity\Marca $marca = null)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Marca 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, parent::IMAGE_BRAND_PRIZE_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Equipo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Equipo
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(parent::IMAGE_BRAND_PRIZE_FOLDER);
    }

}
