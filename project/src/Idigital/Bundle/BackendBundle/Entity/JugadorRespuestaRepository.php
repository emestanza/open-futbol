<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * JugadorRespuestaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JugadorRespuestaRepository extends EntityRepository
{
    public function getLastAnswer($userId)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('r')
                ->from('BackendBundle:JugadorRespuesta', 'r')
                ->where("r.jugador = " . $userId)
                ->orderBy("r.created_at", "DESC")
                ->setMaxResults(1)
                ->setFirstResult(0);

        $result = $qb->getQuery()->getOneOrNullResult();
        
        return $result;
    }
}
