<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partido
 *
 * @ORM\Table(name="partido")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\PartidoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Partido
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="LigaCampeonato", inversedBy="partidos")
     * @ORM\JoinColumn(name="campeonato_id", referencedColumnName="id", nullable=false)
     * */
    private $campeonato;

    /**
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumn(name="equipo1_id", referencedColumnName="id", nullable=false)
     * */
    private $equipoUno;

    /**
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumn(name="equipo2_id", referencedColumnName="id", nullable=false)
     * */
    private $equipoDos;

    /**
     * @ORM\OneToMany(targetEntity="Apuesta", mappedBy="partido")
     * */
    private $apuestas;

    /**
     * @ORM\ManyToOne(targetEntity="Etapa", inversedBy="partidos")
     * @ORM\JoinColumn(name="etapa_id", referencedColumnName="id", nullable=false)
     **/
    private $etapa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaPartido", type="datetime")
     */
    private $fechaPartido;

    /**
     * @var string
     *
     * @ORM\Column(name="score_uno", type="string", length=2, nullable = true)
     */
    private $scoreUno;

    /**
     * @var string
     *
     * @ORM\Column(name="score_dos", type="string", length=2, nullable = true)
     */
    private $scoreDos;

    /**
     * @var string
     *
     * @ORM\Column(name="estadio", type="string", length=255)
     */
    private $estadio;

    /**
     * @var integer
     *
     * @ORM\Column(name="puntos_otorgados", type="smallint", nullable = true)
     */
    private $puntosOtorgados;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cerrado", type="boolean", options={"default" = 0})
     */
    private $cerrado;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaPartido
     *
     * @param \DateTime $fechaPartido
     * @return Partido
     */
    public function setFechaPartido($fechaPartido)
    {
        $this->fechaPartido = $fechaPartido;

        return $this;
    }

    /**
     * Get fechaPartido
     *
     * @return \DateTime 
     */
    public function getFechaPartido()
    {
        return $this->fechaPartido;
    }

    /**
     * Set scoreUno
     *
     * @param string $scoreUno
     * @return Partido
     */
    public function setScoreUno($scoreUno)
    {
        $this->scoreUno = $scoreUno;

        return $this;
    }

    /**
     * Get scoreUno
     *
     * @return string 
     */
    public function getScoreUno()
    {
        return $this->scoreUno;
    }

    /**
     * Set scoreDos
     *
     * @param string $scoreDos
     * @return Partido
     */
    public function setScoreDos($scoreDos)
    {
        $this->scoreDos = $scoreDos;

        return $this;
    }

    /**
     * Get scoreDos
     *
     * @return string 
     */
    public function getScoreDos()
    {
        return $this->scoreDos;
    }

    /**
     * Set estadio
     *
     * @param string $estadio
     * @return Partido
     */
    public function setEstadio($estadio)
    {
        $this->estadio = $estadio;

        return $this;
    }

    /**
     * Get estadio
     *
     * @return string 
     */
    public function getEstadio()
    {
        return $this->estadio;
    }

    /**
     * Set cerrado
     *
     * @param boolean $cerrado
     * @return Partido
     */
    public function setCerrado($cerrado)
    {
        $this->cerrado = $cerrado;

        return $this;
    }

    /**
     * Get cerrado
     *
     * @return boolean 
     */
    public function getCerrado()
    {
        return $this->cerrado;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Partido
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Partido
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add apuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas
     * @return Partido
     */
    public function addApuesta(\Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas)
    {
        $this->apuestas[] = $apuestas;

        return $this;
    }

    /**
     * Remove apuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas
     */
    public function removeApuesta(\Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas)
    {
        $this->apuestas->removeElement($apuestas);
    }

    /**
     * Get apuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApuestas()
    {
        return $this->apuestas;
    }

    /**
     * Set equipoUno
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Equipo $equipoUno
     * @return Partido
     */
    public function setEquipoUno(\Idigital\Bundle\BackendBundle\Entity\Equipo $equipoUno = null)
    {
        $this->equipoUno = $equipoUno;

        return $this;
    }

    /**
     * Get equipoUno
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Equipo 
     */
    public function getEquipoUno()
    {
        return $this->equipoUno;
    }

    /**
     * Set equipoDos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Equipo $equipoDos
     * @return Partido
     */
    public function setEquipoDos(\Idigital\Bundle\BackendBundle\Entity\Equipo $equipoDos = null)
    {
        $this->equipoDos = $equipoDos;

        return $this;
    }

    /**
     * Get equipoDos
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Equipo 
     */
    public function getEquipoDos()
    {
        return $this->equipoDos;
    }

    /**
     * Set campeonato
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonato
     * @return Partido
     */
    public function setCampeonato(\Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonato)
    {
        $this->campeonato = $campeonato;

        return $this;
    }

    /**
     * Get campeonato
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato 
     */
    public function getCampeonato()
    {
        return $this->campeonato;
    }

    /**
     * Set puntosOtorgados
     *
     * @param integer $puntosOtorgados
     * @return Partido
     */
    public function setPuntosOtorgados($puntosOtorgados)
    {
        $this->puntosOtorgados = $puntosOtorgados;

        return $this;
    }

    /**
     * Get puntosOtorgados
     *
     * @return integer 
     */
    public function getPuntosOtorgados()
    {
        return $this->puntosOtorgados;
    }


    /**
     * Set etapa
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Etapa $etapa
     * @return Partido
     */
    public function setEtapa(\Idigital\Bundle\BackendBundle\Entity\Etapa $etapa = null)
    {
        $this->etapa = $etapa;

        return $this;
    }

    /**
     * Get etapa
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Etapa 
     */
    public function getEtapa()
    {
        return $this->etapa;
    }
}
