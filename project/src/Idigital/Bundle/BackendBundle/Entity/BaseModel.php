<?php

namespace Idigital\Bundle\BackendBundle\Entity;

/**
 * Bass class to be used for the model classes
 */
class BaseModel
{

    /**
     * constant to set the base folder to file uploads
     */
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads';

    /**
     * constant to set the team folder to file uploads
     */
    const IMAGE_TEAM_FOLDER = '/equipos';

    /**
     * constant to set the brand folder to file uploads
     */
    const IMAGE_BRAND_FOLDER = '/marcas';

    /**
     * constant to set the user folder to file uploads
     */
    const IMAGE_USERS_FOLDER = '/usuarios';

    /**
     * constant to set the prize folder to file uploads
     */
    const IMAGE_BRAND_PRIZE_FOLDER = '/marcapremios';

    /**
     * constant to set the championship folder to file uploads
     */
    const IMAGE_CHAMPIONSHIP_FOLDER = '/campeonatos';

    /**
     * constant to set the tipo reto folder to file uploads
     */
    const IMAGE_TYPECHALLENGE_FOLDER = '/tiporetos';
    
    /**
     * constant to set the tipo reto folder to file uploads
     */
    const IMAGE_INDEXCONTENT_FOLDER = '/content';

    /**
     * Get the web path of a file
     * 
     * @param type $folder name of the folder to use
     * @return string
     */
    public function getWebPathBase($folder)
    {
        return null === $this->fileName ? null : $this->getUploadDir($folder) . '/' . $this->getUuid() . '/' . $this->fileName;
    }

    /**
     * Get the absolute directory path where uploaded documents should be saved
     * 
     * @param type $basepath base path of the app
     * @param type $folder folder name
     * @return string
     */
    protected function getUploadRootDir($basepath, $folder)
    {
        return $basepath . self::SERVER_PATH_TO_IMAGE_FOLDER . $folder;
    }

    /**
     * Get the upload address for web path
     * 
     * @param type $folder
     * @return string
     */
    protected function getUploadDir($folder)
    {
        return self::SERVER_PATH_TO_IMAGE_FOLDER . $folder;
    }

}
