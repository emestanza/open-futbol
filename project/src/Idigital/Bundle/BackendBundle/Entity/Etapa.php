<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etapa
 *
 * @ORM\Table(name ="etapa")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\EtapaRepository")
 */
class Etapa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * @ORM\OneToMany(targetEntity="Etapa", mappedBy="etapa")
     **/
    private $partidos;

    
    public function __construct() {
        $this->partidos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Etapa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Add partidos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Etapa $partidos
     * @return Etapa
     */
    public function addPartido(\Idigital\Bundle\BackendBundle\Entity\Etapa $partidos)
    {
        $this->partidos[] = $partidos;

        return $this;
    }

    /**
     * Remove partidos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Etapa $partidos
     */
    public function removePartido(\Idigital\Bundle\BackendBundle\Entity\Etapa $partidos)
    {
        $this->partidos->removeElement($partidos);
    }

    /**
     * Get partidos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPartidos()
    {
        return $this->partidos;
    }
}
