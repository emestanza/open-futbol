<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Marca
 *
 * @ORM\Table(name="marca")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\MarcaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Marca extends BaseModel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;
    
    /**
     * object to be used for file uploading
     * @var type UpdateFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=100, nullable = true)
     */
    public $fileName;

    /**
     * @ORM\OneToMany(targetEntity="MarcaPremio", mappedBy="marca")
     * @ORM\OrderBy({"created_at" = "DESC"})
     * */
    private $premios;

    
    /**
     * @ORM\OneToMany(targetEntity="Reto", mappedBy="sponsor")
     **/
    private $retos;
    
    public function __construct()
    {
        $this->premios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->retos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="r_sociales", type="string", length=255, nullable = true)
     */
    private $rSociales;

    /**
     * @var string
     *
     * @ORM\Column(name="r_colaboradores", type="string", length=255, nullable = true)
     */
    private $rColaboradores;

    /**
     * @var integer
     *
     * @ORM\Column(name="r_estrellas", type="smallint", nullable = true)
     */
    private $rEstrellas;

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_retos", type="smallint")
     */
    private $cantRetos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deshabilitado", type="boolean", options={"default" = 0})
     */
    private $deshabilitado = 0;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Marca
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Marca
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Marca
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Marca
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Marca
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string 
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set representantes
     *
     * @param string $representantes
     * @return Marca
     */
    public function setRepresentantes($representantes)
    {
        $this->representantes = $representantes;

        return $this;
    }

    /**
     * Get representantes
     *
     * @return string 
     */
    public function getRepresentantes()
    {
        return $this->representantes;
    }

    /**
     * Set rSociales
     *
     * @param string $rSociales
     * @return Marca
     */
    public function setRSociales($rSociales)
    {
        $this->rSociales = $rSociales;

        return $this;
    }

    /**
     * Get rSociales
     *
     * @return string 
     */
    public function getRSociales()
    {
        return $this->rSociales;
    }

    /**
     * Set rColaboradores
     *
     * @param string $rColaboradores
     * @return Marca
     */
    public function setRColaboradores($rColaboradores)
    {
        $this->rColaboradores = $rColaboradores;

        return $this;
    }

    /**
     * Get rColaboradores
     *
     * @return string 
     */
    public function getRColaboradores()
    {
        return $this->rColaboradores;
    }

    /**
     * Set rEstrellas
     *
     * @param integer $rEstrellas
     * @return Marca
     */
    public function setREstrellas($rEstrellas)
    {
        $this->rEstrellas = $rEstrellas;

        return $this;
    }

    /**
     * Get rEstrellas
     *
     * @return integer 
     */
    public function getREstrellas()
    {
        return $this->rEstrellas;
    }

    /**
     * Set cantRetos
     *
     * @param integer $cantRetos
     * @return Marca
     */
    public function setCantRetos($cantRetos)
    {
        $this->cantRetos = $cantRetos;

        return $this;
    }

    /**
     * Get cantRetos
     *
     * @return integer 
     */
    public function getCantRetos()
    {
        return $this->cantRetos;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Marca
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Marca
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Add premios
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios
     * @return Marca
     */
    public function addPremio(\Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios)
    {
        $this->premios[] = $premios;

        return $this;
    }

    /**
     * Remove premios
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios
     */
    public function removePremio(\Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios)
    {
        $this->premios->removeElement($premios);
    }

    /**
     * Get premios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPremios()
    {
        return $this->premios;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    
    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, parent::IMAGE_BRAND_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }
    

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Equipo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Equipo
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(parent::IMAGE_BRAND_FOLDER);
    }

    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return Marca
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    /**
     * Add retos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Reto $retos
     * @return Marca
     */
    public function addReto(\Idigital\Bundle\BackendBundle\Entity\Reto $retos)
    {
        $this->retos[] = $retos;

        return $this;
    }

    /**
     * Remove retos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Reto $retos
     */
    public function removeReto(\Idigital\Bundle\BackendBundle\Entity\Reto $retos)
    {
        $this->retos->removeElement($retos);
    }

    /**
     * Get retos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRetos()
    {
        return $this->retos;
    }
}
