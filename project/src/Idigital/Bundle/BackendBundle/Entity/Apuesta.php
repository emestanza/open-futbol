<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Apuesta
 *
 * @ORM\Table(name="apuesta")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\ApuestaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Apuesta
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Partido", inversedBy="apuestas")
     * @ORM\JoinColumn(name="partido_id", referencedColumnName="id", nullable=false)
     */
    private $partido;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User", inversedBy="apuestas")
     * @ORM\JoinColumn(name="jugador_id", referencedColumnName="id", nullable=false)
     */
    private $jugador;

    /**
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumn(name="equipo1_id", referencedColumnName="id", nullable=false)
     * */
   // private $equipoUno;

    /**
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumn(name="equipo2_id", referencedColumnName="id", nullable=false)
     * */
    //private $equipoDos;

    /**
     * @var string
     *
     * @ORM\Column(name="apuestaScore1", type="string", length=2)
     */
    private $apuestaScore1;

    /**
     * @var string
     *
     * @ORM\Column(name="apuestaScore2", type="string", length=2)
     */
    private $apuestaScore2;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="gano", type="smallint", options={"default" = 0})
     */
    private $gano;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set apuestaScore1
     *
     * @param string $apuestaScore1
     * @return Apuesta
     */
    public function setApuestaScore1($apuestaScore1)
    {
        $this->apuestaScore1 = $apuestaScore1;

        return $this;
    }

    /**
     * Get apuestaScore1
     *
     * @return string 
     */
    public function getApuestaScore1()
    {
        return $this->apuestaScore1;
    }

    /**
     * Set apuestaScore2
     *
     * @param string $apuestaScore2
     * @return Apuesta
     */
    public function setApuestaScore2($apuestaScore2)
    {
        $this->apuestaScore2 = $apuestaScore2;

        return $this;
    }

    /**
     * Get apuestaScore2
     *
     * @return string 
     */
    public function getApuestaScore2()
    {
        return $this->apuestaScore2;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Apuesta
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Apuesta
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set jugador
     *
     * @param \Application\Sonata\UserBundle\Entity\User $jugador
     * @return Apuesta
     */
    public function setJugador(\Application\Sonata\UserBundle\Entity\User $jugador = null)
    {
        $this->jugador = $jugador;

        return $this;
    }

    /**
     * Get jugador
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getJugador()
    {
        return $this->jugador;
    }

    /**
     * Set partido
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Partido $partido
     * @return Apuesta
     */
    public function setPartido(\Idigital\Bundle\BackendBundle\Entity\Partido $partido)
    {
        $this->partido = $partido;

        return $this;
    }

    /**
     * Get partido
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Partido 
     */
    public function getPartido()
    {
        return $this->partido;
    }


    /**
     * Set gano
     *
     * @param boolean $gano
     * @return Apuesta
     */
    public function setGano($gano)
    {
        $this->gano = $gano;

        return $this;
    }

    /**
     * Get gano
     *
     * @return boolean 
     */
    public function getGano()
    {
        return $this->gano;
    }
}
