<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Equipo
 *
 * @ORM\Table(name="equipo")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\EquipoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Equipo extends BaseModel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="LigaCampeonato", inversedBy="equipos")
     * @ORM\JoinTable(name="equipo_campeonato")
     * */
    private $campeonatos;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=50)
     */
    private $pais;

    /**
     * objtect to be used for file uploading
     * @var type UpdateFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=100, nullable = true)
     */
    public $fileName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deshabilitado", type="boolean", options={"default" = 0})
     */
    private $deshabilitado = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct()
    {
        $this->campeonatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Equipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set pais
     *
     * @param string $pais
     * @return Equipo
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Equipo
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Equipo
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Add campeonatos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos
     * @return Equipo
     */
    public function addCampeonato(\Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos)
    {
        $this->campeonatos[] = $campeonatos;

        return $this;
    }

    /**
     * Remove campeonatos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos
     */
    public function removeCampeonato(\Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos)
    {
        $this->campeonatos->removeElement($campeonatos);
    }

    /**
     * Get campeonatos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampeonatos()
    {
        return $this->campeonatos;
    }

    /**
     * to string method
     * @return type
     */
    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, parent::IMAGE_TEAM_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Equipo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Equipo
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(parent::IMAGE_TEAM_FOLDER);
    }

    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return Equipo
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    
    public function getPaisName(){
        
        if ($this->pais != "" && $this->pais != null)
            return \Idigital\Bundle\BackendBundle\Admin\Utils::$countries[$this->pais];
        else
            return "";
        
    }
}
