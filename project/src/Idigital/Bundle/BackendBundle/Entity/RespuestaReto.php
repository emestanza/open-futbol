<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RespuestaReto
 *
 * @ORM\Table(name="respuesta_reto")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\RespuestaRetoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class RespuestaReto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PreguntaReto", inversedBy="respuestas")
     * @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id", nullable=false)
     * */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=255)
     */
    private $respuesta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="correcto", type="boolean", options={"default" = 0})
     */
    private $correcto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="listar", type="boolean", options={"default" = 1})
     */
    private $list = true;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     * @return RespuestaReto
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set correcto
     *
     * @param boolean $correcto
     * @return RespuestaReto
     */
    public function setCorrecto($correcto)
    {
        $this->correcto = $correcto;

        return $this;
    }

    /**
     * Get correcto
     *
     * @return boolean 
     */
    public function getCorrecto()
    {
        return $this->correcto;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return RespuestaReto
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return RespuestaReto
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set pregunta
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\PreguntaReto $pregunta
     * @return RespuestaReto
     */
    public function setPregunta(\Idigital\Bundle\BackendBundle\Entity\PreguntaReto $pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\PreguntaReto 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set list
     *
     * @param boolean $list
     * @return RespuestaReto
     */
    public function setList($list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return boolean 
     */
    public function getList()
    {
        return $this->list;
    }

}
