<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * LigaCampeonato
 *
 * @ORM\Table(name="liga_campeonato")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\LigaCampeonatoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class LigaCampeonato extends BaseModel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;

    /**
     * object to be used for file uploading
     * @var type UpdateFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=100, nullable = true)
     */
    public $fileName;

    /**
     * @ORM\ManyToMany(targetEntity="Equipo", mappedBy="campeonatos")
     * */
    private $equipos;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Sonata\UserBundle\Entity\User", mappedBy="campeonatos")
     * */
    private $jugadoresRegistrados;

    /**
     * @ORM\OneToMany(targetEntity="Partido", mappedBy="campeonato")
     * @ORM\OrderBy({"fechaPartido" = "ASC"})
     * */
    private $partidos;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
    
    /**
     * @var text
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=50)
     */
    private $pais;

    /**
     * @var date
     *
     * @ORM\Column(name="fecha_inicio", type="date")
     */
    private $fechaInicio;

    /**
     * @var date
     *
     * @ORM\Column(name="fecha_fin", type="date")
     */
    private $fechaFin;

    /**
     * @var date
     *
     * @ORM\Column(name="num_fechas", type="string", length=2)
     */
    private $numFechas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deshabilitado", type="boolean", options={"default" = 0})
     */
    private $deshabilitado = 0;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct()
    {
        $this->equipos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jugadoresRegistrados = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return LigaCampeonato
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set pais
     *
     * @param string $pais
     * @return LigaCampeonato
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        if ($this->pais != "" && $this->pais != null)
            return \Idigital\Bundle\BackendBundle\Admin\Utils::$countries[$this->pais];
        else
            return $this->pais;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return LigaCampeonato
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return LigaCampeonato
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Add equipos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Equipo $equipos
     * @return LigaCampeonato
     */
    public function addEquipo(\Idigital\Bundle\BackendBundle\Entity\Equipo $equipos)
    {
        $this->equipos[] = $equipos;

        return $this;
    }

    /**
     * Remove equipos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Equipo $equipos
     */
    public function removeEquipo(\Idigital\Bundle\BackendBundle\Entity\Equipo $equipos)
    {
        $this->equipos->removeElement($equipos);
    }

    /**
     * Get equipos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEquipos()
    {
        return $this->equipos;
    }

    /**
     * Add jugadoresRegistrados
     *
     * @param \Application\Sonata\UserBundle\Entity\User $jugadoresRegistrados
     * @return LigaCampeonato
     */
    public function addJugadoresRegistrado(\Application\Sonata\UserBundle\Entity\User $jugadoresRegistrados)
    {
        $this->jugadoresRegistrados[] = $jugadoresRegistrados;

        return $this;
    }

    /**
     * Remove jugadoresRegistrados
     *
     * @param \Application\Sonata\UserBundle\Entity\User $jugadoresRegistrados
     */
    public function removeJugadoresRegistrado(\Application\Sonata\UserBundle\Entity\User $jugadoresRegistrados)
    {
        $this->jugadoresRegistrados->removeElement($jugadoresRegistrados);
    }

    /**
     * Get jugadoresRegistrados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJugadoresRegistrados()
    {
        return $this->jugadoresRegistrados;
    }

    /**
     * Add partidos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Partido $partidos
     * @return LigaCampeonato
     */
    public function addPartido(\Idigital\Bundle\BackendBundle\Entity\Partido $partidos)
    {
        $this->partidos[] = $partidos;

        return $this;
    }

    /**
     * Remove partidos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Partido $partidos
     */
    public function removePartido(\Idigital\Bundle\BackendBundle\Entity\Partido $partidos)
    {
        $this->partidos->removeElement($partidos);
    }

    /**
     * Get partidos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPartidos()
    {
        return $this->partidos;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return LigaCampeonato
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return LigaCampeonato
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set numFechas
     *
     * @param integer $numFechas
     * @return LigaCampeonato
     */
    public function setNumFechas($numFechas)
    {
        $this->numFechas = $numFechas;

        return $this;
    }

    /**
     * Get numFechas
     *
     * @return integer 
     */
    public function getNumFechas()
    {
        return $this->numFechas;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, parent::IMAGE_CHAMPIONSHIP_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Equipo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Equipo
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(parent::IMAGE_CHAMPIONSHIP_FOLDER);
    }


    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return LigaCampeonato
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return LigaCampeonato
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
