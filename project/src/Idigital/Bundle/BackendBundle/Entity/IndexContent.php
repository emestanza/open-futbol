<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * IndexContent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\IndexContentRepository")
 */
class IndexContent extends BaseModel
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=400)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50)
     */
    private $uuid;

    /**
     * objtect to be used for file uploading
     * @var type UpdateFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=100, nullable = true)
     */
    public $fileName;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setModifiedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->setModifiedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return IndexContent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return IndexContent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return IndexContent
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, parent::IMAGE_INDEXCONTENT_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Equipo
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(parent::IMAGE_INDEXCONTENT_FOLDER);
    }

    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return Equipo
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Equipo
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Equipo
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

}
