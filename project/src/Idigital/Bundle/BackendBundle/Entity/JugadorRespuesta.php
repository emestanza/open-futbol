<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JugadorRespuesta
 *
 * @ORM\Table(name="jugador_respuesta")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\JugadorRespuestaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class JugadorRespuesta
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="RespuestaReto")
     * @ORM\JoinColumn(name="respuesta_id", referencedColumnName="id", nullable=false)
     * */
    private $respuesta;

    /**
     * @ORM\ManyToOne(targetEntity="\Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="jugador_id", referencedColumnName="id", nullable=false)
     * */
    private $jugador;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta_abierta", type="text", nullable=true)
     */
    private $respuestaAbierta;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return JugadorRespuesta
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return JugadorRespuesta
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Set jugador
     *
     * @param \Application\Sonata\UserBundle\Entity\User $jugador
     * @return JugadorRespuesta
     */
    public function setJugador(\Application\Sonata\UserBundle\Entity\User $jugador)
    {
        $this->jugador = $jugador;

        return $this;
    }

    /**
     * Get jugador
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getJugador()
    {
        return $this->jugador;
    }

    /**
     * Set respuesta
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuesta
     * @return JugadorRespuesta
     */
    public function setRespuesta(\Idigital\Bundle\BackendBundle\Entity\RespuestaReto $respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\RespuestaReto 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set respuestaAbierta
     *
     * @param string $respuestaAbierta
     * @return JugadorRespuesta
     */
    public function setRespuestaAbierta($respuestaAbierta)
    {
        $this->respuestaAbierta = $respuestaAbierta;

        return $this;
    }

    /**
     * Get respuestaAbierta
     *
     * @return string 
     */
    public function getRespuestaAbierta()
    {
        return $this->respuestaAbierta;
    }

}
