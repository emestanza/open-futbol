<?php

namespace Idigital\Bundle\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * Reto
 *
 * @ORM\Table(name="reto")
 * @ORM\Entity(repositoryClass="Idigital\Bundle\BackendBundle\Entity\RetoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Reto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TipoReto", inversedBy="retos")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id", nullable=false)
     * */
    private $tipoReto;

    /**
     * @ORM\OneToMany(targetEntity="PreguntaReto", mappedBy="reto")
     * @ORM\OrderBy({"ordenPrioridad" = "ASC"})
     * */
    private $preguntas;

    public function __construct()
    {
        $this->preguntas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="duracion", type="smallint")
     */
    private $duracion;

    /**
     * @var integer
     *
     * @ORM\Column(name="gemas_otorgadas", type="smallint")
     */
    private $gemasOtorgadas;

    /**
     * @var string
     *
     * @ORM\Column(name="orden_prioridad", type="smallint", nullable = true)
     */
    private $ordenPrioridad = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deshabilitado", type="boolean", options={"default" = 0})
     */
    private $deshabilitado = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Marca", inversedBy="retos")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id", nullable=false)
     * */
    private $sponsor;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $modified_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Reto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Reto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set duracion
     *
     * @param integer $duracion
     * @return Reto
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return integer 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set gemasOtorgadas
     *
     * @param integer $gemasOtorgadas
     * @return Reto
     */
    public function setGemasOtorgadas($gemasOtorgadas)
    {
        $this->gemasOtorgadas = $gemasOtorgadas;

        return $this;
    }

    /**
     * Get gemasOtorgadas
     *
     * @return integer 
     */
    public function getGemasOtorgadas()
    {
        return $this->gemasOtorgadas;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Reto
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set modified_at
     *
     * @param \DateTime $modifiedAt
     * @return Reto
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modified_at = $modifiedAt;

        return $this;
    }

    /**
     * Get modified_at
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setModifiedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Reto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set tipoReto
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\TipoReto $tipoReto
     * @return Reto
     */
    public function setTipoReto(\Idigital\Bundle\BackendBundle\Entity\TipoReto $tipoReto = null)
    {
        $this->tipoReto = $tipoReto;

        return $this;
    }

    /**
     * Get tipoReto
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\TipoReto 
     */
    public function getTipoReto()
    {
        return $this->tipoReto;
    }

    /**
     * Add preguntas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\PregustaReto $preguntas
     * @return Reto
     */
    public function addPregunta(\Idigital\Bundle\BackendBundle\Entity\PreguntaReto $preguntas)
    {
        $this->preguntas[] = $preguntas;

        return $this;
    }

    /**
     * Remove preguntas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\PregustaReto $preguntas
     */
    public function removePregunta(\Idigital\Bundle\BackendBundle\Entity\PreguntaReto $preguntas)
    {
        $this->preguntas->removeElement($preguntas);
    }

    /**
     * Get preguntas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPreguntas()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('deshabilitado', false));

        return $this->preguntas->matching($criteria);
    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Set deshabilitado
     *
     * @param boolean $deshabilitado
     * @return Reto
     */
    public function setDeshabilitado($deshabilitado)
    {
        $this->deshabilitado = $deshabilitado;

        return $this;
    }

    /**
     * Get deshabilitado
     *
     * @return boolean 
     */
    public function getDeshabilitado()
    {
        return $this->deshabilitado;
    }

    /**
     * Set sponsor
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Marca $sponsor
     * @return Reto
     */
    public function setSponsor(\Idigital\Bundle\BackendBundle\Entity\Marca $sponsor = null)
    {
        $this->sponsor = $sponsor;

        return $this;
    }

    /**
     * Get sponsor
     *
     * @return \Idigital\Bundle\BackendBundle\Entity\Marca 
     */
    public function getSponsor()
    {
        return $this->sponsor;
    }

    /**
     * Set ordenPrioridad
     *
     * @param integer $ordenPrioridad
     * @return Reto
     */
    public function setOrdenPrioridad($ordenPrioridad)
    {
        $this->ordenPrioridad = $ordenPrioridad;

        return $this;
    }

    /**
     * Get ordenPrioridad
     *
     * @return integer 
     */
    public function getOrdenPrioridad()
    {
        return $this->ordenPrioridad;
    }

}
