<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Marca Entity admin class managed by Sonata Admin Bundle
 */
class MarcaAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');
        if ($image && ($webPath = $image->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }

        $formMapper
                ->add('nombre')
                ->add('facebook')
                ->add('email')
                ->add('website')
                ->add('twitter')
                ->add('rSociales', null, array("required" => false))
                ->add('rColaboradores', null, array("required" => false))
                ->add('rEstrellas', null, array("required" => false))
                ->add('cantRetos')
                ->add('file', 'file', $fileFieldOptions)
                ->add('deshabilitado', "hidden")
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('nombre')
                ->add('email')
                ->add('deshabilitado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('nombre')
                ->add('facebook')
                ->add('email')
                ->add('website')
                ->add('deshabilitado')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'Disable' => array(
                            'template' => 'BackendBundle:CRUD:list__action_disable.html.twig'
                        )
                    )
                ))

        ;
    }

    /**
     * Executes a validation of some values of the entity
     * 
     * @param \Sonata\AdminBundle\Validator\ErrorElement $errorElement
     * @param type $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('email')
                ->assertEmail(array("message" => "Ingrese un correo electrónico válido"))
                ->end();
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $jugador
     */
    public function prePersist($jugador)
    {
        if ($jugador->getUuid() == "" || $jugador->getUuid() == null) {
            $jugador->setUuid(uniqid());
        }

        $this->saveFile($jugador);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $jugador
     */
    public function preUpdate($jugador)
    {
        $this->saveFile($jugador);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $jugador
     */
    public function saveFile($jugador)
    {
        $basepath = $this->getRequest()->getBasePath();
        $jugador->upload($basepath);
    }

    /**
     * Configure admina actions
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
