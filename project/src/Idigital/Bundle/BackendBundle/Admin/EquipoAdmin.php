<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Equipo Entity admin class managed by Sonata Admin Bundle
 */
class EquipoAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Equipo instance
        $equipoObj = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');

        if ($equipoObj && ($webPath = $equipoObj->getWebPath())) {

            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }

        $formMapper
                ->add('nombre')
                ->add('pais', 'choice', array(
                    'choices' => Utils::$countries, 'label' => 'País'))
                ->add('file', 'file', $fileFieldOptions)
                ->add('campeonatos', 'sonata_type_model', array('class' => 'Idigital\Bundle\BackendBundle\Entity\LigaCampeonato', "multiple" => true, 'by_reference' => false, "required" => false))
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('nombre')
                ->add('pais', 'doctrine_orm_string', array(), 'choice', array('choices' => Utils::$countries))
                ->add('deshabilitado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('nombre')
                ->add('paisName', null, array("label" => "País"))
                ->add('deshabilitado')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'Disable' => array(
                            'template' => 'BackendBundle:CRUD:list__action_disable.html.twig'
                        )
                    )
                ))
        ;
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $equipo
     */
    public function prePersist($equipo)
    {
        if ($equipo->getUuid() == "" || $equipo->getUuid() == null) {
            $equipo->setUuid(uniqid());
        }

        $this->saveFile($equipo);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $equipo
     */
    public function preUpdate($equipo)
    {
        $this->saveFile($equipo);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $equipo
     */
    public function saveFile($equipo)
    {
        $basepath = $this->getRequest()->getBasePath();
        $equipo->upload($basepath);
    }

    /**
     * Configure admin action 
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
