<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class RespuestaRetoAdmin extends Admin
{

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
                ->add('pregunta', 'sonata_type_model')
                ->add('respuesta')
                ->add('correcto', "checkbox", array("required" => false))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('pregunta')
                ->add('respuesta')
                ->add('correcto')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('respuesta')
                ->add('pregunta')
                ->add('correcto')
        ;
    }

}
