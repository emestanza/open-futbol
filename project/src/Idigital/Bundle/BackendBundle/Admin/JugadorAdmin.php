<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Jugador Entity admin class managed by Sonata Admin Bundle
 */
class JugadorAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');
        if ($image && ($webPath = $image->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }

        $formMapper
                ->add('nombre')
                ->add('apellido')
                ->add('genero', "choice", array("choices" => Utils::$generoArr))
                ->add('usuario')
                ->add('correo')
                ->add('dni')
                ->add('estadoCivil', "choice", array("choices" => Utils::$edoCivilArr))
                ->add('fechaNacimiento', 'date', array('years' => range(1960, date('Y')), 'format' => 'dd-MMMM-yyyy'))
                ->add('file', 'file', $fileFieldOptions)
                ->add('paisNacimiento', 'choice', array(
                    'choices' => Utils::$countries,))
                ->add('campeonatos', 'sonata_type_model', array('class' => 'Idigital\Bundle\BackendBundle\Entity\LigaCampeonato', "multiple" => true, 'by_reference' => false, "required" => false))
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('nombre')
                ->add('pais', 'doctrine_orm_string', array(), 'choice', array('choices' => Utils::$countries))
                ->add('deshabilitado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('nombre')
                ->add('usuario')
                ->add('paisNacimiento')
                ->add('deshabilitado')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'Disable' => array(
                            'template' => 'BackendBundle:CRUD:list__action_disable.html.twig'
                        )
                    )
                ))
        ;
    }

    /**
     * Executes a validation of some values of the entity
     * 
     * @param \Sonata\AdminBundle\Validator\ErrorElement $errorElement
     * @param type $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('correo')
                ->assertEmail(array("message" => "Ingrese un correo electrónico válido"))
                ->end()
                ->with('dni')
                ->assertType(array("type" => "digit", "message" => "Ingrese un número de DNI"))
                ->end();
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $jugador
     */
    public function prePersist($jugador)
    {
        if ($jugador->getUuid() == "" || $jugador->getUuid() == null) {
            $jugador->setUuid(uniqid());
        }

        $this->saveFile($jugador);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $jugador
     */
    public function preUpdate($jugador)
    {
        $this->saveFile($jugador);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $jugador
     */
    public function saveFile($jugador)
    {
        $basepath = $this->getRequest()->getBasePath();
        $jugador->upload($basepath);
    }

    /**
     * Configure admina actions
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
