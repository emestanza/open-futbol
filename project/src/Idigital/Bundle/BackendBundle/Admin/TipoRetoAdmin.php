<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * TipoReto Entity admin class managed by Sonata Admin Bundle
 */
class TipoRetoAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');
        if ($image && ($webPath = $image->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }


        $formMapper
                ->add('nombre')
                ->add('descripcion')
                ->add('file', 'file', $fileFieldOptions)
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('nombre')
                ->add('deshabilitado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('nombre')
                ->add('descripcion')
                ->add('deshabilitado')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'Disable' => array(
                            'template' => 'BackendBundle:CRUD:list__action_disable.html.twig'
                        )
                    )
                ))
                
        ;
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $tipoReto
     */
    public function prePersist($tipoReto)
    {
        if ($tipoReto->getUuid() == "" || $tipoReto->getUuid() == null) {
            $tipoReto->setUuid(uniqid());
        }

        $this->saveFile($tipoReto);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $tipoReto
     */
    public function preUpdate($tipoReto)
    {
        $this->saveFile($tipoReto);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $tipoReto
     */
    public function saveFile($tipoReto)
    {
        $basepath = $this->getRequest()->getBasePath();
        $tipoReto->upload($basepath);
    }
    
    /**
     * Configure admina actions
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
