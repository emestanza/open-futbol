<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * LigaCampeonato Entity admin class managed by Sonata Admin Bundle
 */
class LigaCampeonatoAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        // get the current Image instance
        $image = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');
        if ($image && ($webPath = $image->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }


        $formMapper
                ->add('nombre')
                ->add('descripcion')
                ->add('pais', 'choice', array(
                    'choices' => Utils::$countries,))
                ->add('fechaInicio', "date", array("format" => "dd MMMM yyyy"))
                ->add('fechaFin', "date", array("format" => "dd MMMM yyyy"))
                ->add('numFechas')
                ->add('file', 'file', $fileFieldOptions)
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('nombre')
                ->add('pais', 'doctrine_orm_string', array(), 'choice', array('choices' => Utils::$countries))
                ->add('fechaInicio', 'doctrine_orm_callback', array(
                    'label' => 'Fecha Inicio',
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                if (!$value['value']) {
                    return;
                }
                $time = strtotime($value['value']);
                $inputValue = date('Y-m-d', $time);

                $queryBuilder->andWhere("$alias.fechaInicio = :fechaInicio");
                $queryBuilder->setParameter('fechaInicio', $inputValue);
                return true;
            },
                    'field_type' => 'text'
                        ), null, array('attr' => array('class' => 'datepicker')))
                ->add('fechaFin', 'doctrine_orm_callback', array(
                    'label' => 'Fecha Fin',
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                if (!$value['value']) {
                    return;
                }
                $time = strtotime($value['value']);
                $inputValue = date('Y-m-d', $time);

                $queryBuilder->andWhere("$alias.fechaFin = :fechaFin");
                $queryBuilder->setParameter('fechaFin', $inputValue);
                return true;
            },
                    'field_type' => 'text'
                        ), null, array('attr' => array('class' => 'datepicker')))
                ->add('deshabilitado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('nombre')
                ->add('pais')
                ->add('fechaInicio', 'date', array(
                    'pattern' => 'dd MMMM yyyy',
                    'locale' => 'es',
                    'timezone' => 'America/Lima',
                ))
                ->add('fechaFin', 'date', array(
                    'pattern' => 'dd MMMM yyyy',
                    'locale' => 'es',
                    'timezone' => 'America/Lima',
                ))
                ->add('numFechas')
                ->add('deshabilitado')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'Disable' => array(
                            'template' => 'BackendBundle:CRUD:list__action_disable.html.twig'
                        )
                    )
                ))
        ;
    }

    /**
     * Executes a validation of some values of the entity
     * 
     * @param \Sonata\AdminBundle\Validator\ErrorElement $errorElement
     * @param type $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('numFechas')
                ->assertType(array("type" => "digit", "message" => "Ingrese un número de fechas"))
                ->end();
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $ligaCampeonato
     */
    public function prePersist($ligaCampeonato)
    {
        if ($ligaCampeonato->getUuid() == "" || $ligaCampeonato->getUuid() == null) {
            $ligaCampeonato->setUuid(uniqid());
        }

        $this->saveFile($ligaCampeonato);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $ligaCampeonato
     */
    public function preUpdate($ligaCampeonato)
    {
        $this->saveFile($ligaCampeonato);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $ligaCampeonato
     */
    public function saveFile($ligaCampeonato)
    {
        $basepath = $this->getRequest()->getBasePath();
        $ligaCampeonato->upload($basepath);
    }

    /**
     * Configure admina actions
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
