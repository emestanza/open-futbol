<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Equipo Entity admin class managed by Sonata Admin Bundle
 */
class EmailNotificationAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name')
                ->add('day', "choice", array("choices" => Utils::$dayArr))
                ->add('hour', "choice", array("choices" => Utils::$hourArr))
                ->add('minute', "choice", array("choices" => Utils::$minuteArr));
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('name')
                ->add('day', 'choice', array('choices' => Utils::$dayArr))
                ->add('hour')
                ->add('minute');
    }

}
