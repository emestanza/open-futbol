<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Equipo Entity admin class managed by Sonata Admin Bundle
 */
class IndexContentAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // get the current Equipo instance
        $contentObj = $this->getSubject();

        // use $fileFieldOptions so we can add other options to the field
        $fileFieldOptions = array('required' => false, 'label' => 'Imagen');

        if ($contentObj && ($webPath = $contentObj->getWebPath())) {

            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request')->getBasePath() . '/' . $webPath;

            // add a 'help' option containing the preview's img tag
            $fileFieldOptions['help'] = '<img src="' . $fullPath . '" style="max-width:150px; max-height:150px;" />';
        }

        $formMapper
                ->add('title')
                ->add('description')
                ->add('file', 'file', $fileFieldOptions);
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('title')
                ->add('description')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('title')
                ->add('description')
        ;
    }

    /**
     * Method to be triggered when orm does a create sentence
     * @param type $equipo
     */
    public function prePersist($equipo)
    {
        if ($equipo->getUuid() == "" || $equipo->getUuid() == null) {
            $equipo->setUuid(uniqid());
        }

        $this->saveFile($equipo);
    }

    /**
     * Method to be triggered when orm does an update sentence
     * @param type $equipo
     */
    public function preUpdate($equipo)
    {
        $this->saveFile($equipo);
    }

    /**
     * This method is used to make the model save a file into the system
     * @param type $equipo
     */
    public function saveFile($equipo)
    {
        $basepath = $this->getRequest()->getBasePath();
        $equipo->upload($basepath);
    }

    /**
     * Configure admin action 
     * @param \Sonata\AdminBundle\Route\RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('disable', $this->getRouterIdParameter() . '/disable');
    }

}
