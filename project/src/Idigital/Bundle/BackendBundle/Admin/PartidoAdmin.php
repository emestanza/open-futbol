<?php

namespace Idigital\Bundle\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Partido Entity admin class managed by Sonata Admin Bundle
 */
class PartidoAdmin extends Admin
{

    /**
     * Fields to be shown on create/edit forms
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
                ->add('campeonato', 'sonata_type_model')
                ->add('equipoUno', 'sonata_type_model')
                ->add('equipoDos', 'sonata_type_model')
                ->add('etapa', 'sonata_type_model')
                ->add('fechaPartido', "datetime", array('date_format' => 'dd-MMMM-yyyy HH:mm', 'format' => 'dd-MMMM-yyyy HH:mm'))
                ->add('scoreUno', "text", array("required" => false))
                ->add('scoreDos', "text", array("required" => false))
                ->add('puntosOtorgados', "hidden", array('data' => '0'))
                ->add('estadio')
                ->add('cerrado', 'checkbox', array("required" => false))
        ;
    }

    /**
     * Fields to be shown on filter forms
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('fechaPartido')
                ->add('equipoUno')
                ->add('equipoDos')
                ->add('estadio')
                ->add('cerrado')
        ;
    }

    /**
     * Fields to be shown on lists
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('fechaPartido', 'datetime', array(
                    'pattern' => 'dd MMMM yyyy, hh:mm a',
                    'locale' => 'es',
                    'timezone' => 'America/Lima',
                ))
                ->add('etapa')
                ->add('equipoUno')
                ->add('equipoDos')
                ->add('estadio')
                ->add('cerrado')
        ;
    }

}
