<?php

namespace Idigital\Bundle\BackendBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CRUDController extends Controller
{

    public function disableAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $message = "";

        if ($object->getDeshabilitado()) {
            $object->setDeshabilitado(false);
            $message = 'Elemento habilitado exitosamente';
        } else {
            $object->setDeshabilitado(true);
            $message = 'Elemento deshabilitado exitosamente';
        }

        $this->admin->update($object);
        $this->addFlash('sonata_flash_success', $message);

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

}
