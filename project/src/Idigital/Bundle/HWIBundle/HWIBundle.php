<?php

namespace Idigital\Bundle\HWIBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HWIBundle extends Bundle
{

    public function getParent()
    {
        return 'HWIOAuthBundle';
    }

}
