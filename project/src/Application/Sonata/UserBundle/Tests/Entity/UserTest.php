<?php

namespace Application\Sonata\UserBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Validator\Validation;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTest
 *
 * @author enmanuel
 */
class UserTest extends \PHPUnit_Framework_TestCase
{

    private $validator;

    protected function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()
                ->enableAnnotationMapping()
                ->getValidator();
    }

    //put your code here
    public function testUserData()
    {
        $userObj = new User();

        $userObj->setFirstname("Enmanuel");
        $userObj->setLastname("Mestanza");
        $userObj->setUsername("emestanza");
        $userObj->setEmail("emestanza");
        $userObj->setPassword("12345");

        $listaErrores = $this->validator->validate($userObj);
        $this->assertGreaterThan(0, $listaErrores->count(), 'errores: '.$listaErrores[0]);
    }

}
