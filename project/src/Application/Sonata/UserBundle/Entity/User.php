<?php

/**
 * This file is part of the <name> project.
 *
 * (c) <yourname> <youremail>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Idigital\BackendBundle\Entity\Apuesta;
use Idigital\Bundle\BackendBundle\Entity\MarcaPremio;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{

    /**
     * @var integer $id
     */
    protected $id;

    /**
     * object to be used for file uploading
     * @var type UpdateFile
     */
    protected $file;

    /**
     * @var string
     */
    private $dni;

    /**
     * @var string
     */
    private $estadoCivil;

    /**
     * @var integer
     */
    private $puntos = 0;

    /**
     * @var integer
     */
    private $monedas = 150;

    /**
     * @var integer
     */
    private $gemas = 0;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $premios;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $apuestas;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $campeonatos;

    public function __construct()
    {
        parent::__construct();
        $this->apuestas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->premios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->campeonatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return User
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     * @return User
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string 
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set puntos
     *
     * @param integer $puntos
     * @return User
     */
    public function setPuntos($puntos)
    {
        $this->puntos = $puntos;

        return $this;
    }

    /**
     * Get puntos
     *
     * @return integer 
     */
    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Set monedas
     *
     * @param integer $monedas
     * @return User
     */
    public function setMonedas($monedas)
    {
        $this->monedas = $monedas;

        return $this;
    }

    /**
     * Get monedas
     *
     * @return integer 
     */
    public function getMonedas()
    {
        return $this->monedas;
    }

    /**
     * Set gemas
     *
     * @param integer $gemas
     * @return User
     */
    public function setGemas($gemas)
    {
        $this->gemas = $gemas;

        return $this;
    }

    /**
     * Get gemas
     *
     * @return integer 
     */
    public function getGemas()
    {
        return $this->gemas;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return User
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return User
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Add apuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas
     * @return User
     */
    public function addApuesta(\Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas)
    {
        $this->apuestas[] = $apuestas;

        return $this;
    }

    /**
     * Remove apuestas
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas
     */
    public function removeApuesta(\Idigital\Bundle\BackendBundle\Entity\Apuesta $apuestas)
    {
        $this->apuestas->removeElement($apuestas);
    }

    /**
     * Get apuestas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApuestas()
    {
        return $this->apuestas;
    }

    /**
     * Add premios
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios
     * @return User
     */
    public function addPremio(\Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios)
    {
        $this->premios[] = $premios;

        return $this;
    }

    /**
     * Remove premios
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios
     */
    public function removePremio(\Idigital\Bundle\BackendBundle\Entity\MarcaPremio $premios)
    {
        $this->premios->removeElement($premios);
    }

    /**
     * Get premios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPremios()
    {
        return $this->premios;
    }

    /**
     * Add campeonatos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos
     * @return User
     */
    public function addCampeonato(\Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos)
    {
        $this->campeonatos[] = $campeonatos;

        return $this;
    }

    /**
     * Remove campeonatos
     *
     * @param \Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos
     */
    public function removeCampeonato(\Idigital\Bundle\BackendBundle\Entity\LigaCampeonato $campeonatos)
    {
        $this->campeonatos->removeElement($campeonatos);
    }

    /**
     * Get campeonatos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampeonatos()
    {
        return $this->campeonatos;
    }

    /**
     * constant to set the user folder to file uploads
     */
    const IMAGE_USERS_FOLDER = '/usuarios';

    /**
     * constant to set the base folder to file uploads
     */
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads';

    /**
     * Method to be used by the admin class to get the web path
     * @return string
     */
    public function getWebPath()
    {
        return $this->getWebPathBase(self::IMAGE_USERS_FOLDER);
    }

    /**
     * Get the web path of a file
     * 
     * @param type $folder name of the folder to use
     * @return string
     */
    public function getWebPathBase($folder)
    {
        return null === $this->fileName ? null : $this->getUploadDir($folder) . '/' . $this->getUuid() . '/' . $this->fileName;
    }

    /**
     * Get the upload address for web path
     * 
     * @param type $folder
     * @return string
     */
    protected function getUploadDir($folder)
    {
        return self::SERVER_PATH_TO_IMAGE_FOLDER . $folder;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     * 
     * @param type $basepath
     * @return type void
     */
    public function upload($basepath)
    {

        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        //saves the image into the folder 
        $this->getFile()->move($this->getUploadRootDir($basepath, self::IMAGE_USERS_FOLDER) . "/" . $this->getUuid(), $this->getFile()->getClientOriginalName());

        // set the path property to the filename where you've saved the file
        $this->fileName = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * Get the absolute directory path where uploaded documents should be saved
     * 
     * @param type $basepath base path of the app
     * @param type $folder folder name
     * @return string
     */
    protected function getUploadRootDir($basepath, $folder)
    {
        return $basepath . self::SERVER_PATH_TO_IMAGE_FOLDER . $folder;
    }

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * @var string
     */
    private $pais;

    /**
     * Set pais
     *
     * @param string $pais
     * @return User
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        return $this->pais;
    }

}
