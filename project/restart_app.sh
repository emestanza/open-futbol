#!/bin/sh
# This is a comment

echo "Reiniciando app:"

if [ "$1" = "test" ];
then
    echo "Parameters y htaccess - testing:"
    cp /home/openfutboladmin/idigital_files/test/parameters.yml /home/openfutboladmin/public_html/pruebas/open-futbol/project/app/config/parameters.yml
    cp -fr /home/openfutboladmin/idigital_files/test/.htaccess /home/openfutboladmin/public_html/pruebas/open-futbol/project/web/.htaccess
fi

if [ "$1" = "prod" ];
then
    echo "Parameters y htaccess - produccion:"
    cp /home/openfutboladmin/idigital_files/prod/parameters.yml /home/openfutboladmin/public_html/produccion/open-futbol/project/app/config/parameters.yml
    cp -fr /home/openfutboladmin/idigital_files/prod/.htaccess /home/openfutboladmin/public_html/produccion/open-futbol/project/web/.htaccess
fi

if [ "$2" = "hard" ];
then

    echo "Borrando Base de Datos"
    php app/console doctrine:database:drop --force

    echo "Creando Base de Datos"
    php app/console doctrine:database:create

    echo "Creando tablas"
    php app/console doctrine:schema:update --force
    
    echo "Agregando Trigger"

    if [ "$1" = "test" ];
    then
        mysql -h localhost -uopenfutb_admin -pJquJVjexDweHJD56qxdL openfutb_tst < /home/openfutboladmin/idigital_files/triggers/update_points_tr.sql
    fi

    if [ "$1" = "prod" ];
    then
        mysql -h localhost -uopenfutb_admin -pJquJVjexDweHJD56qxdL openfutb_app < /home/openfutboladmin/idigital_files/triggers/update_points_tr.sql
    
    fi

    if [ "$1" = "dev" ];
    then
          mysql -h localhost -uroot -pstrtok openfutbol_db < /home/enmanuel/openfutbol_docs/triggers/update_points_tr.sql
    fi
      
  
fi

echo "Limpiando carpetas cache-logs"
rm -rf app/cache/*
rm -rf app/logs/*


HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

if [ "$1" = "dev" ];
then
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:enmanuel:rwX app/cache app/logs
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:enmanuel:rwX app/cache app/logs
    php app/console cache:clear --env=$1

    echo "Assets y Assetics"
    php app/console assets:install web --env=$1 --symlink

else

    setfacl -dR -m u:"$HTTPDUSER":rwX -m u:root:rwX app/cache app/logs
    setfacl -R -m u:"$HTTPDUSER":rwX -m u:root:rwX app/cache app/logs
    php app/console cache:clear --env=$1 --no-debug

    echo "Assets y Assetics"
    php app/console assets:install web --env=$1 --symlink

fi


if [ "$2" = "hard" ];
then

    echo "Limpiando carpeta web/uploads"
    sudo rm web/uploads -Rf
    sudo mkdir web/uploads
    sudo chmod 777 web/uploads -Rf

    echo "Creando Usuario Admin"
    php app/console fos:user:create admin admin@email.com strtok
    php app/console fos:user:promote admin@email.com ROLE_SONATA_ADMIN

fi
